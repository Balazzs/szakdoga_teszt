# Szakdolgozat program #

Hibaterjedés vizsgálata fizikai modellekben

A program célja numerikus hibák terjedésének vizsgálata algoritmusokban (így fizikai modellek implementációjában).

A numerikus hibák terjedésének követése, vizualizálása, végeredményre vett hatásuk elemzése.

Ehhez a kód képes felépíteni műveleti fákat (**A**bstract **S**yntrax **T**ree), eltárolni ezeket, kiértékelni, vizualizálni (, illetve könnyedén továbbfejleszthető szövegként megadott algoritmusok beolvasására egy parser megírásával, vagy például az adott képlet LaTeX megjelenítésére, egyéb általános célra, etc.)

### Futtatás ###

A program VS2017-el lett készítve, ám több ponton is ellenőrizve lett a platform és fordító függetlenség, így az elérhető tesztgépeken futott Windows (VS/g++) és Linux (Arch/Ubuntu, g++) rendszereken is.

A kétszeres double, avagy quadruple pontossághoz egy quadruple precision library (a platformfüggetlenség kedvéért) enyhén módosított verzióját használtam, ez elérhető a letöltések között. A Dbldbl típusok qd_real template-esítéséhez ez a library szükséges.

**Figyelem! A program végleges állapotában több órás szimulációt futtat.** Amennyiben az eredmények nem lényegesek, pusztán a program kinézetének és futásának megértése a cél elegendő lehet csupán a teszt eseteket átnézni (UnitTests.h)

### Alapvető felépítés ###

A program rendelkezik egy erősen template-esített Szam osztállyal, ez egy számot és a hozzá tartozó hibát reprezentálja. 4 különböző template-esített módszerrel (AbsErr, RepEst, CENA, Dbldbl, lsd. Szam.h eleje), ezeken implementálva vannak a szükséges műveletek, az érték és (lokális/műveleti szintű) hibaterjedés.

A tényleges algoritmus szintű terjedés vizsgálatához felépítünk egy műveleti fát (AST3.hpp), amely általános típusú konstans számokból, változókból, definiált al-fákból (azaz értékmegadásból / kb. egyszerű nem rekurzív függvényhívás) és a rajtuk végrehajtott műveletekből áll. A fa képes végigjárni és rekurzívan kiértékelni magát, vizualizálni magát kapott VisuInfo objektum (lambdával megadható az egyes node-okhoz rendelendő vizualizációs információ) segítségével. A vizualizáció nem közvetlen, DOT file generálást jelent, amely könnyedén megjeleníthető graphviz-el.

A program komplexitásának jelentős része az AST implementálása, az ehhez szükséges Contextek (adott scopeban létező definiált változók) megfelelő továbbítása és egyszerűsítése, amik által a futásidő nagyságrendekkel növekedett.