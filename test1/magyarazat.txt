teszt1 az egyes hibasz�mol�si t�pusokra
dd_real ellen�rz�ssel


els� oszlop hiba �rt�ke 2^-53 - ban (double epsilon)
m�sodik oszlop abs(abs(dd_real ellen�rz�m�velet - �rt�k (double)) - abs(hiba) )

5 m�velet:

+
-
*
/
sqrt



-------
 
DblDouble v�rt hiba egzakt 0 (dd_real ellen�rizve dd_reallel), t�k�letes

CENA +,-,* egzakt, / hib�ja < epsilon (~1 �rt�kek, max hibahat�r <1*epsilon, belef�r, de furcs�n nagy), sqrt 1e-32 nagys�grend

RepEst ugyanaz, mint CENA, hisz csak abszol�t�rt�k k�l�nbs�g (1. oszlop pozit�v)

AbsErr rendben van, minden borzalmas, mint ahogy kell lennie