#pragma once

#include "AST3.hpp"

template <class T>
class AST;


template <class T>
AST<T> sqrt(const AST<T>& a);

template <class T>
AST<T> pow(const AST<T>& a, const AST<T>& b);

template <class T>
AST<T> log(const AST<T>& a);

template <class T>
AST<T> sin(const AST<T>& a);

template <class T>
AST<T> cos(const AST<T>& a);


template <class T>
class AST {

	bool isReady = false;
	bool isEvaluated = false;
	T value;

	AST_<T> ast;


	//Friends
	friend bool unit_tests();

	friend AST sqrt<>(const AST& a);
	friend AST pow<>(const AST& a, const AST& b);
	friend AST log<>(const AST& a);
	friend AST sin<>(const AST& a);
	friend AST cos<>(const AST& a);

	template <class U>
	friend class AST;

private:
	AST(const AST_<T>& ast) : ast(ast) {}

	AST(AST_<T>&& ast) : ast(std::move(ast)) {}

public:

	//Operators///////////////////////////////
	//////////////////////////////////////////

	AST operator+(const AST& other)
	{
		return AST(ast + other.ast);
	}

	AST& operator+=(const AST& other)
	{
		*this = operator+(other);
		return *this;
	}

	AST operator-()
	{
		return AST(-ast);
	}

	AST operator-(const AST& other)
	{
		return AST(ast - other.ast);
	}

	AST& operator-=(const AST& other)
	{
		*this = operator-(*this, other);
		return *this;
	}

	AST operator*(const AST& other)
	{
		return AST(ast * other.ast);
	}

	AST& operator*=(const AST& other)
	{
		*this = operator*(*this, other);
		return *this;
	}

	AST operator/(const AST& other)
	{
		return AST(ast / other.ast);
	}

	AST& operator/=(const AST& other)
	{
		*this = operator/(*this, other);
		return *this;
	}


	//Integration/////////////////////////////
	//////////////////////////////////////////

	AST integral(const std::string& xVar, const AST& x0, const AST& x1, const AST& dx)
	{
		return AST(ast.integral(xVar, x0.ast, x1.ast, dx.ast));
	}

	AST& integrate(const std::string& xVar, const AST& x0, const AST& x1, const AST& dx)
	{
		ast.integrate(xVar, x0.ast, x1.ast, dx.ast);
		isReady = false;
		isEvaluated = false;

		return *this;
	}


	//Constant////////////////////////////////
	//////////////////////////////////////////

	AST(const T& value) : ast(AST_<T>(value))
	{
		isReady = true;
	}

	AST(T&& value) : ast(std::move(value)) { }

	//Variable////////////////////////////////
	//////////////////////////////////////////

	AST(const std::string& name) : ast(AST_<T>(name)) { }

	//The expression is simply a variable with name <name>
	//and it has the <ast> bound to <name>
	AST(const std::string& name, const AST& ast) : ast( AST_<T>(name, ast.ast) ) { }

	AST(const std::string& name, AST&& ast) : ast( AST_<T>(name, std::move(ast.ast)) ) {};

	AST nameAs(const std::string& name)
	{
		return AST( AST_<T>(name, *this) );
	}

	AST& bind(const std::vector< std::pair< std::string, T> >& pairs)
	{
		return operator=( bound(pairs) );
	}

	AST& bind(const std::vector< std::pair< std::string, AST_<T>> >& pairs)
	{
		return operator=( bound(pairs));
	}

	AST bound(const std::vector< std::pair< std::string, AST_<T>> >& pairs)
	{
		return AST( ast.bound(pairs) );
	}

	AST bound(std::vector< std::pair< std::string, AST_<T>> >&& pairs)
	{
		return AST( ast.bound( std::move(pairs) ) );
	}

	AST boundC(const std::map< std::string, T>& pairs)
	{
		std::vector< std::pair < std::string, AST_<T> > > temp;

		for (auto& x : pairs)
			temp.push_back({ x.first, AST_<T>(x.second) });

		return AST(ast.bound(std::move(temp)));
	}

	AST boundC(std::map< std::string, T >&& pairs)
	{
		std::vector< std::pair < std::string, AST_<T> > > temp;

		for (auto& x : pairs)
			temp.push_back({ std::move(x.first), AST_<T>(std::move(x.second)) });

		return AST(ast.bound(std::move(temp)));
	}

	//Binds integral variables, they don't have values
	AST boundI(const std::vector<std::string>& vars)
	{
		return AST(ast.bound(vars));
	}

	bool setLinks()
	{
		isReady = true;
		return ast.setLinks(Scope<T>());
	}

	T evaluate()
	{
		auto lambdas = ast.getLambdas({});

		//Check for unset variables
		if (lambdas.size())
		{
			std::string varsString = std::accumulate(lambdas.begin(), lambdas.end(), std::string("Linking error in AST evaluation ( unset variables: "), [](const std::string& left, const std::string& right) { return left + "\"" + right + "\" "; }) + ")";
#ifdef AST_EXCEPTION
			throw std::invalid_argument( varsString.c_str() );
#else
			std::cerr << varsString << std::endl;
			return NAN;
#endif
		}

		if (!isReady)
			setLinks();

		return ast.evaluate();
	}

	void visualize(const std::string& fname)
	{
		VisuInfo<T> viz(fname);
		visualize( viz );
	}

	void visualize(VisuInfo<T>& visInfo)
	{
		//Open file
		visInfo.open();

		//If we want to use the values we need them
		if(visInfo.calculateValues)
		{
			if (!isReady)
				setLinks();
			
			if(!isEvaluated)
				ast.evaluateToMemory();
		}
		
		//A global ID given to the visualization as reference to increment and keep track of the elements
		int ID = 0;
		//Visualize
		ast.visualize(visInfo, ID, 0);

		//Close file
		visInfo.close();
	}
	
	T evaluate(std::map< std::string, T > params)
	{
		//Create Scope from std::map
		Scope<T> scope;

		for (auto& x : params)
			scope.add(x.first, &x.second);

		//Check whether we have all the needed (and no more) variables set
		{
			std::set<std::string> lambdas = ast.getLambdas({}), paramsStr;

			for (const auto& p : params)
				paramsStr.insert(p.first);

			if (paramsStr != lambdas)
			{
				std::set<std::string> needed, extra;

				std::set_difference(lambdas.begin(), lambdas.end(), paramsStr.begin(), paramsStr.end(), std::inserter(needed, needed.end()));
				std::set_difference(paramsStr.begin(), paramsStr.end(), lambdas.begin(), lambdas.end(), std::inserter(extra, extra.end()));

				std::string str = "Linking error in AST evaluation (";

				std::string neededString = std::accumulate(needed.begin(), needed.end(), std::string(""), [](const std::string& left, const std::string& right) { return left + "\"" + right + "\" "; });
				std::string extraString = std::accumulate(extra.begin(), extra.end(), std::string(""), [](const std::string& left, const std::string& right) { return left + "\"" + right + "\" "; });

				if (neededString.size())
					str += " unset variables: " + neededString;

				if (extraString.size())
					str += " extra variables: " + extraString;

				str += ")";

#ifdef AST_EXCEPTION
				throw std::invalid_argument(str.c_str());
#else
				std::cerr << str << std::endl;
				
				//If we have everything needed set and some extra 
				//then preferably don't ruin the evaluation, let it be there and just give a warning
				if(neededString.size())
					return NAN;
#endif
			}
		}	

		//We added our own params so its not ready
		isReady = false;

		//Evaluate
		return ast.evaluate(scope);
	}

	AST& simplify()
	{
		ast.simplify();
		isReady = false;
		isEvaluated = false;
		return *this;
	}

	AST simplified()
	{
		return AST(ast.simplified());
	}

public:

	AST& operator=(const AST& other)
	{
		ast = other.ast;
		isReady = false;
		isEvaluated = false;

		return *this;
	}

	AST& operator=(AST&& other)
	{
		ast = std::move(other.ast);
		isReady = false;
		isEvaluated = false;

		return *this;
	}

	AST(const AST& other) : ast(other.ast)
	{
		isReady = false;
		isEvaluated = false;
	}

	AST(AST&& other) : ast(std::move(other.ast))
	{
		isReady = false;
		isEvaluated = false;
	}

	template <class U, bool castAllValues = false>
	AST<U> recast(const std::function< U (const T&) >& func) const
	{
		return (AST<U>( ast.template recast< U, castAllValues >(func) ) );
	}

	bool operator==(const AST& other) const
	{
		return other.ast == ast && isReady == other.isReady && isEvaluated == isEvaluated;
	}

	bool operator!=(const AST& other) const
	{
		return !operator==(other);
	}

	operator AST_<T>() const { return ast; };

};

template <class T>
AST<T> sqrt(const AST<T>& a)
{
	return AST<T>(sqrt(a.ast));
}

template <class T>
AST<T> pow(const AST<T>& a, const AST<T>& b)
{
	return AST<T>(pow(a.ast, b.ast));
}

template <class T>
AST<T> log(const AST<T>& a)
{
	return AST<T>(log(a.ast));
}

template <class T>
AST<T> sin(const AST<T>& a)
{
	return AST<T>(sin(a.ast));
}

template <class T>
AST<T> cos(const AST<T>& a)
{
	return AST<T>(cos(a.ast));
}
