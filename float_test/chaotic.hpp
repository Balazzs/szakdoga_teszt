#include "ASTWrapper.hpp"
#include "Vector.h"

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#include <future>


//Typedes for easier use
typedef Szam<Method::DblDouble, double, dd_real> T_Dbldbl;
typedef Szam<Method::CENA, double, double> T_CENA;
typedef Szam<Method::RepEst, double, double> T_RepEst;
typedef Szam<Method::AbsErr, double, double> T_AbsErr;

typedef Szam<Method::DblDouble, float, double> T_F2_Dbldbl;
typedef Szam<Method::CENA, float, double> T_F_CENA;
typedef Szam<Method::RepEst, float, double> T_F_RepEst;
typedef Szam<Method::AbsErr, float, double> T_F_AbsErr;


//const double pi = 4 * atan(1.0);

const double g = 9.8;        // acceleration of gravity

double L = 1.0;              // length of pendulum

template <class T>
Vector<T> dp(const Vector<T>& x)
{
	const T theta1 = x[1];
	const T p1 = x[2];
	const T theta2 = x[3];
	const T p2 = x[4];

	const T COS = cos(theta1 - theta2);

	//double p1 = 1.0 / 6.0 * 1.0 * L * L * (8 * omega1 + 3 * omega2 * COS);
	//double p2 = 1.0 / 6.0 * 1.0 * L * L * (2 * omega2 + 3 * omega1 * COS);

	Vector<T> f(5);
	f[0] = 1;
	f[1] = T(6.0) / (T(1.0) * T(L) * T(L)) * (T(2.0) * p1 - T(3.0) * COS * p2) / (T(16.0) - T(9.0) * COS*COS);
	f[3] = T(6.0) / (T(1.0) * T(L) * T(L)) * (T(8.0) * p2 - T(3.0) * COS * p1) / (T(16.0) - T(9.0) * COS*COS);
	
	f[2] = -T(0.5) * T(L)* T(L) * (f[1] * f[3] * sin(theta1 - theta2) + T(3.0) * T(g) / T(L) * sin(theta1) );
	f[4] = -T(0.5) * T(L)* T(L) * (-f[1] * f[3] * sin(theta1 - theta2) + T(g) / T(L) * sin(theta2) );

	return f;
}

//TODO
template < class T, class F>
void RK4Step(Vector<T>& x, const T& dt, const F& deriv )
{
    Vector<T> k1, k2, k3, k4;

    k1 = deriv(x);
    k2 = deriv(x + k1 * dt / T(2.0));
    k3 = deriv(x + k2 * dt / T(2.0));
    k4 = deriv(x + k3 * dt);

    x += dt / T(6.0) * ( k1 + T(2.0) * k2 + T(2.0) * k3 + k4 );
}

template <class T>
std::vector< Vector<T> > getPendulumData(double dt, double tMax)
{
    std::vector< Vector<T> > data;

    Vector<T> xx(5);
	xx[0] = 0;
	xx[1] = 3;
	xx[2] = 0.0;
	xx[3] = 3;
	xx[4] = 0.0;
    
	while (valueOf(xx[0]) < tMax) {
        for(int i = 0; i < 10; i++)
            RK4Step(xx, T(dt), &dp<T>);
		data.push_back(xx);
	}
    
    return data;
}

template <class T>
void writePend( std::ostream& os, Vector<T> p )
{
    os << "\t" << valueOf(p[1]) << "\t" << errorOf(p[1]) << "\t" << valueOf(p[3]) << "\t" << errorOf(p[3]);
}

void pendulum(const std::string& fname)
{
	double tMax = 20;
	double dt = 0.001;

	//double accuracy = 1e-6;
	ofstream dataFile(fname);
	
    auto dataA = getPendulumData<T_F_AbsErr>(dt, tMax);
    auto dataC = getPendulumData<T_F_CENA>(dt, tMax);
    auto dataD = getPendulumData<T_F2_Dbldbl>(dt, tMax);
    auto dataR = getPendulumData<T_F_RepEst>(dt, tMax);
	
    for(size_t i = 0; i < dataA.size(); i++)
	{
        dataFile << valueOf(dataA[i][0]);
        writePend(dataFile, dataA[i]);
        writePend(dataFile, dataC[i]);
        writePend(dataFile, dataD[i]);
        writePend(dataFile, dataR[i]);
        dataFile << std::endl;
	}

	dataFile.close();
}