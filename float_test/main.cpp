//Throw exceptions instead of returning NAN and writing to cerr
//#define AST_EXCEPTION

#include <iostream>
#include "UnitTests.h"
#include "plots.hpp"
#include "chaotic.hpp"
#include "algorithms.h"

#include <iomanip>
#include <sstream>

//Typedes for easier use
typedef Szam<Method::DblDouble, double, dd_real> T_Dbldbl;
typedef Szam<Method::CENA, double, double> T_CENA;
typedef Szam<Method::RepEst, double, double> T_RepEst;
typedef Szam<Method::AbsErr, double, double> T_AbsErr;

typedef Szam<Method::DblDouble, float, dd_real> T_F_Dbldbl;
typedef Szam<Method::CENA, float, double> T_F_CENA;
typedef Szam<Method::RepEst, float, double> T_F_RepEst;
typedef Szam<Method::AbsErr, float, double> T_F_AbsErr;

//Constants
const double c = 299792458, G = 6.67408e-11, mSun = 1.9891e30;
const double SunToSec = mSun * G / c / c / c;

using A = AST<dd_real>;

A expression(0.0), expr_noSN(0.0);

template <bool needsSNMultiplier = true, bool doSpam = false>
A createAST()
{
	//Constants
	A PI(dd_real("3.1415926535897932384626433832795028841971693"));
	A gamma(dd_real("0.57721566490153286060651209008240243104215933593992"));

	//Variables

	A f0("f0"), df( "df");

	A m1("m1"), m2("m2");
	A mu = A("eta", m1*m2 / pow((m1 + m2), A(2))), M = A("M", m1 + m2);

	A f("f"), tC("tC"), fC = A(1.0) / (A(6.0) * sqrt( A(6.0) ) * PI * M), phiC("phiC");
	A v = A("v", pow(PI * M * f, A(1.0) / A(3.0))), v0 = A(1.0);

	//Phase
	A psi = A(2) * PI * f * tC - A(2) * phiC - PI / A(4) + A(3) / (A(128) * mu * pow(v, A(5))) * (A(1) \
		+ (A(3715) / A(756) + A(55) / A(9) * mu) * v*v \
		- A(16) * PI * v*v*v \
		+ (A(15293365) / A(508032) + A(27145) / A(504) * mu + A(3085) / A(72) * mu*mu) * pow(v, A(4))\
		+ PI * (A(38645) / A(756) - A(65) / A(9) * mu) * (A(1) + A(3) * log(v / v0)) * pow(v, A(5))\
		+ (A(11583231236531.0) / A(4694215680.0) \
			- A(640) / A(3) * PI * PI \
			- A(6848) / A(21) * (gamma + log(A(4) * v)) \
			+ (-A(15335597827.0) / A(3048192.0) + A(2255) / A(12) * PI*PI) * mu \
			+ A(76055) / A(1728) * mu*mu \
			- A(127825) / A(1296) * mu*mu*mu) * pow(v, A(6)) \
		+ PI * (A(77096675.0) / A(254016) + A(378515) / A(1512) * mu - A(74045) / A(756) * mu * mu) * pow(v, A(7))
		);

	psi = psi.nameAs("psi");

	//Wave in frequency domain without phase and amplitude (its gonna be normalised)
	A h = A("A_f", pow(f, A(-7) / A(6)));

	//Noise power spectrum
	A x = A("x", f / A(300));
	
	A Sn(0.0);
	
	if (needsSNMultiplier)
		Sn = (A(1.6e-49) * (A(300) * pow(f, A(-17)) + A(7) * pow(f / A(50), A(-6)) + A(24) * pow(A(300) * x / A(90), A(-3.45)) - A(3.5) / pow(x, A(2)) + A(110) * (A(1.02) - A(1.08) * x * x + A(0.54) * pow(x, A(4))) / (A(1) + A(0.21) * x * x)));
	else
		Sn = ((A(300) * pow(f, A(-17)) + A(7) * pow(f / A(50), A(-6)) + A(24) * pow(A(300) * x / A(90), A(-3.45)) - A(3.5) / pow(x, A(2)) + A(110) * (A(1.02) - A(1.08) * x * x + A(0.54) * pow(x, A(4))) / (A(1) + A(0.21) * x * x)));

	//The 2 different psi
	auto psi_1 = A("psi1");
	auto psi_2 = A("psi2");

	//Bind fC context
	fC.bind({ { "m1", A("m1_2") },{ "m2", A("m2_2") } });

	//Cutoff freq
	if (doSpam)
		std::cout << "Cutoff frequency: " << fC.evaluate() << std::endl;

	//The product of the two templates
	auto inner_product = h * h * (cos(psi_1)*cos(psi_2) + sin(psi_1)*sin(psi_2)).bound({ {"psi1", psi.bound({ { "m1", A("m1_1") },{ "m2", A("m2_1") } }) }, { "psi2", psi.bound({ { "m1", A("m1_2") },{ "m2", A("m2_2") } }) } }) / Sn;

	//Integrand
	auto inner_product_norm = h * h / Sn;

	//Get the normalization factors
	auto norm1 = inner_product_norm.integrate("f", f0, fC, df);

	//Actually its the square root of each norm multiplied, so we should calculate it properly for error's sake //ASK
	//norm1 = sqrt(norm1) * sqrt(norm1);

	inner_product.integrate("f", f0, fC, df);
	
	return inner_product / norm1;
}


void doMassErrorPlots(const std::string& dir)
{
	doGWMassError<Szam<DblDouble, double, double>>({ -0.01, 0.01 }, { -0.01, 0.01 }, 40, 40, 41, 20, dir + "/massErrors_1percent");
	doGWMassError<Szam<DblDouble, double, double>>({ -0.1, 0.1 }, { -0.1, 0.1 }, 40, 40, 41, 20, dir + "/massErrors_10percent");

	doGWMassError<Szam<DblDouble, double, double>>({ -0.01, 0.01 }, { -0.01, 0.01 }, 40, 40, 30, 30, dir + "/massErrors_1percent");
	doGWMassError<Szam<DblDouble, double, double>>({ -0.1, 0.1 }, { -0.1, 0.1 }, 40, 40, 30, 30, dir + "/massErrors_10percent");

	doGWMassError<Szam<DblDouble, double, double>>({ -0.01, 0.01 }, { -0.01, 0.01 }, 40, 40, 5, 50, dir + "/massErrors_1percent");
	doGWMassError<Szam<DblDouble, double, double>>({ -0.1, 0.1 }, { -0.1, 0.1 }, 40, 40, 5, 50, dir + "/massErrors_10percent");


	doGWMassError<T_AbsErr>({ -0.01, 0.01 }, { -0.01, 0.01 }, 40, 40, 41, 20, dir + "/massErrors_1percent");
	doGWMassError<T_AbsErr>({ -0.1, 0.1 }, { -0.1, 0.1 }, 40, 40, 41, 20, dir + "/massErrors_10percent");

	doGWMassError<T_AbsErr>({ -0.01, 0.01 }, { -0.01, 0.01 }, 40, 40, 30, 30, dir + "/massErrors_1percent");
	doGWMassError<T_AbsErr>({ -0.1, 0.1 }, { -0.1, 0.1 }, 40, 40, 30, 30, dir + "/massErrors_10percent");

	doGWMassError<T_AbsErr>({ -0.01, 0.01 }, { -0.01, 0.01 }, 40, 40, 5, 50, dir + "/massErrors_1percent");
	doGWMassError<T_AbsErr>({ -0.1, 0.1 }, { -0.1, 0.1 }, 40, 40, 5, 50, dir + "/massErrors_10percent");


	doGWMassError<T_CENA>({ -0.01, 0.01 }, { -0.01, 0.01 }, 40, 40, 41, 20, dir + "/massErrors_1percent");
	doGWMassError<T_CENA>({ -0.1, 0.1 }, { -0.1, 0.1 }, 40, 40, 41, 20, dir + "/massErrors_10percent");

	doGWMassError<T_CENA>({ -0.01, 0.01 }, { -0.01, 0.01 }, 40, 40, 30, 30, dir + "/massErrors_1percent");
	doGWMassError<T_CENA>({ -0.1, 0.1 }, { -0.1, 0.1 }, 40, 40, 30, 30, dir + "/massErrors_10percent");

	doGWMassError<T_CENA>({ -0.01, 0.01 }, { -0.01, 0.01 }, 40, 40, 5, 50, dir + "/massErrors_1percent");
	doGWMassError<T_CENA>({ -0.1, 0.1 }, { -0.1, 0.1 }, 40, 40, 5, 50, dir + "/massErrors_10percent");
}

void doSomeMorePlots(const std::string& dir)
{
	//Detecting one as the other
	
	doGWDetectionError<T_Dbldbl>({ 20, 40 }, {20, 40}, 30, 30, 41, 41, dir + "/detection");
	doGWDetectionError<T_AbsErr>({ 20, 40 }, { 20, 40 }, 30, 30, 41, 41, dir + "/detection");
	doGWDetectionError<T_RepEst>({ 20, 40 }, { 20, 40 }, 30, 30, 41, 41, dir + "/detection");
	doGWDetectionError<T_CENA>({ 20, 40 }, { 20, 40 }, 30, 30, 41, 41, dir + "/detection");

	//Check with different masses and 0 initial error
	doGWMassFloatingError<T_AbsErr>({ 10 * SunToSec, 60 * SunToSec }, { 10 * SunToSec, 60 * SunToSec }, 40, 40, dir + "/massFloatingErrors");
	doGWMassFloatingError<T_Dbldbl>({ 10 * SunToSec, 60 * SunToSec }, { 10 * SunToSec, 60 * SunToSec }, 40, 40, dir + "/massFloatingErrors");
	doGWMassFloatingError<T_CENA>({ 10 * SunToSec, 60 * SunToSec }, { 10 * SunToSec, 60 * SunToSec }, 40, 40, dir + "/massFloatingErrors");
	doGWMassFloatingError<T_RepEst>({ 10 * SunToSec, 60 * SunToSec }, { 10 * SunToSec, 60 * SunToSec }, 40, 40, dir + "/massFloatingErrors");
	
	doGWMassFloatingError<T_F_AbsErr, false>({ 10 * SunToSec, 60 * SunToSec }, { 10 * SunToSec, 60 * SunToSec }, 40, 40, dir + "/massFloatingErrors");
	doGWMassFloatingError<T_F_Dbldbl, false>({ 10 * SunToSec, 60 * SunToSec }, { 10 * SunToSec, 60 * SunToSec }, 40, 40, dir + "/massFloatingErrors");
	doGWMassFloatingError<T_F_CENA, false>({ 10 * SunToSec, 60 * SunToSec }, { 10 * SunToSec, 60 * SunToSec }, 40, 40, dir + "/massFloatingErrors");
	doGWMassFloatingError<T_F_RepEst, false>({ 10 * SunToSec, 60 * SunToSec }, { 10 * SunToSec, 60 * SunToSec }, 40, 40, dir + "/massFloatingErrors");

}

void oneDimensionSimplePlot(const std::string& fname)
{
	std::ofstream fs(fname);

	T_AbsErr a;
	T_CENA c;
	T_Dbldbl d;
	T_RepEst r;

	for (double x = 0.7; x < 1.3; x += 0.001)
	{
		a = x;
		c = x;
		d = x;
		r = x;
		fs << x << "\t" << errorOf(T_AbsErr(1.0) / ( a - 1.0 )) << "\t" << errorOf(T_CENA(1.0) / (c - 1.0)) << "\t" << errorOf(T_Dbldbl(1.0) / (d - 1.0)) << "\t" << errorOf(T_RepEst(1.0) / (r - 1.0)) << std::endl;
	}

	fs.close();
}

void oneDimensionStartingErrorPlot(const std::string& fname)
{
	std::ofstream fs(fname);

	T_AbsErr a;
	T_CENA c;
	T_Dbldbl d;
	T_RepEst r;

	for (double x = 0.8; x < 1.4; x += 0.001)
	{
		a = T_AbsErr(x, 0.1);
		c = T_CENA(x, 0.1);
		d = T_Dbldbl(x, 0.1);
		r = T_RepEst(x, 0.1);
		fs << x << "\t" << errorOf(T_AbsErr(1.0) / (a - 1.0)) << "\t" << errorOf(T_CENA(1.0) / (c - 1.0)) << "\t" << errorOf(T_Dbldbl(1.0) / (d - 1.0)) << "\t" << errorOf(T_RepEst(1.0) / (r - 1.0)) << std::endl;
	}

	fs.close();
}

int main()
{
	std::cout << "TESTS: " << (unit_tests() ? "PASSED" : "FAILED") << std::endl;

	//Set the expression
	expression = createAST().boundC({{"f0", 10}, { "df", 1e-2 }});
	expr_noSN = createAST<false>().boundC({{"f0", 10}, { "df", 1e-2 }});

	//We need extra precision for stdio
	std::cout << std::setprecision(20);


	//Some test contexts
	std::vector< std::pair<std::string, AST_<T_CENA> > > con_({ { "m1_1", AST_<T_CENA>(40 * SunToSec) },{ "m2_1", AST_<T_CENA>(20 * SunToSec) },{ "m1_2", AST_<T_CENA>(42 * SunToSec) },{ "m2_2", AST_<T_CENA>(18 * SunToSec) },{ "tC", AST_<T_CENA>(0.0) },{ "phiC", AST_<T_CENA>(0.0) } });
	std::map<std::string, T_CENA> con({ { "m1_1", T_CENA(40 * SunToSec) },{ "m2_1", T_CENA(20 * SunToSec) },{ "m1_2", T_CENA(42 * SunToSec) },{ "m2_2", T_CENA(18 * SunToSec) },{ "tC", T_CENA(0.0) },{ "phiC", T_CENA(0.0) } });
	std::map<std::string, T_Dbldbl> conDbldbl({ { "m1_1", T_Dbldbl(40 * SunToSec) },{ "m2_1", T_Dbldbl(20 * SunToSec) },{ "m1_2", T_Dbldbl(42 * SunToSec) },{ "m2_2", T_Dbldbl(18 * SunToSec) },{ "tC", T_Dbldbl(0.0) },{ "phiC", T_Dbldbl(0.0) } });

	//auto start = std::chrono::high_resolution_clock::now();
	//Do something
	//std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start).count() << std::endl;


	////////////////////////////////
	//Higher precision constructor//
	////////////////////////////////
	//dd_real a = std::string("3.141592653589793238462643383");

	//std::cout << a << std::endl;
	//std::cout << T_CENA(dd_real(std::string("3.141592653589793238462643383"))) << std::endl;

	////////////////
	//Binding test//
	////////////////
	
	//std::cout << (A("var") + A("var2")).evaluate({ { "var", 3 },{ "var2",2 },{ "pityu", 137 } }) << std::endl;
	
	////////////////
	//df test///////
	////////////////
	
	//Reinit expression trees with unset df
	//expression = createAST().boundC({ { "f0", 10 } });
	//expr_noSN = createAST<false>().boundC({ { "f0", 10 } });

	//std::cout << getExpression<T_CENA>(true).boundC({{"df", 1e-2}}).evaluate(con) << std::endl;
	//std::cout << getExpression<T_CENA>(true).boundC({ {"df", 1e-2} }).boundC(con).simplify().evaluate() << std::endl;
	//std::cout << getExpression<T_CENA>(true).boundC({{"df", 1e-3}}).simplify().evaluate(con) << std::endl;
	//std::cout << getExpression<T_CENA>(true).boundC({{"df", 1e-4}}).simplify().evaluate(con) << std::endl;

	//std::cout << "Dbldbl" << std::endl;	

	//std::cout << getExpression<T_Dbldbl>(true).boundC(conDbldbl).boundC({{"df", 1e-2}}).evaluate() << std::endl;
	//std::cout << getExpression<T_Dbldbl>(true).boundC(conDbldbl).boundC({{"df", 1e-2}}).simplify().evaluate() << std::endl;
	//std::cout << getExpression<T_Dbldbl>(true).boundC(conDbldbl).boundC({{"df", 1e-3}}).simplify().evaluate() << std::endl;
	//std::cout << getExpression<T_Dbldbl>(true).boundC(conDbldbl).boundC({{"df", 1e-4}}).simplify().evaluate() << std::endl;
	
	////////////////
	//Do big plots//
	////////////////

	//doMassErrorPlots("Data/Big plots");
	//doSomeMorePlots("Data/Big plots");
	
	////////////////
	//VISUALIZE/////
	////////////////
	/*
	getExpression<T_CENA>(true).boundC({ {"df", 1e-2} }).boundC(con).visualize("out.viz");

	auto viz = VisuInfo<T_CENA>("out2.viz", true, false, [](const T_CENA& t){ return errorOf(t); }, [](const T_CENA& t)
	{
		std::ostringstream iss;
		iss << std::scientific << "\\n" << t;
		return iss.str();
	});
	
	getExpression<T_CENA>(true).boundC({ {"df", 1e-2} }).boundC(con).simplify().visualize( viz );
	*/

	//oneDimensionSimplePlot("Data/1D_1.dat");
	//oneDimensionStartingErrorPlot("Data/1D_2.dat");

	illConditioned<T_AbsErr>();
	illConditioned<T_RepEst>();
	illConditioned<T_CENA>();
	illConditioned<T_Dbldbl>();

	doTests<T_AbsErr>();
	doTests<T_RepEst>();
	doTests<T_CENA>();
	doTests<T_Dbldbl>();


	//integral("Data/Integral.dat");

	//pendulum("pend.dat");

	//std::getchar();

	

}