#pragma once

#include "Szam.h"

template<class V, class E>
class Szam<Method::AbsErr, V, E> {

	V value;
	E error;

public:

	static constexpr V eps = std::numeric_limits<V>::epsilon();
	static constexpr int digits = std::numeric_limits<V>::digits;

	Szam()
	{
		this->value = V(0.0);
		this->error = E(0.0);
	}

	template <class V_, typename = std::enable_if_t< std::is_arithmetic< V_ >::value > >
	Szam(V_ value, bool forceError = false)
	{
		this->value = (V) value;
		this->error = (value == floor(value) || !forceError) ? (E) abs(value - V(value)) : (E) (eps * abs(value) );
	}

	template <class V_, typename = std::enable_if_t< std::is_arithmetic< V_ >::value > >
	Szam(V_ value, E relError)
	{
		this->value = (V) value;
		this->error = (E) (abs(relError * value) + abs( value - V(value) )  );
	}

	Szam(AbsoluteError dummy)
	{
		this->value = V(0.0);
		this->error = E(0.0);
	}

	template <class V_, typename = std::enable_if_t< std::is_arithmetic< V_ >::value > >
	Szam(AbsoluteError dummy, V_ value, E error = 0.0)
	{
		this->value = (V) value;
		this->error = (E) (error + abs(value - V(value)) );
	}

	Szam& operator+=(const Szam& other)
	{
		const V newValue = value + other.value;
		error = (E) (other.error + eps * abs(newValue) + error);
		value = newValue;
		return *this; 
	}

	Szam operator+(const Szam& other) const
	{
		return Szam(*this) += other;
	}

	Szam operator-()
	{
		return Szam(AbsoluteError(), -value, error);
	}

	Szam& operator-=(const Szam& other)
	{
		const V newValue = value - other.value;
		error = (E) (eps * abs(newValue) + error + other.error);
		value = newValue;
		return *this;
	}

	Szam operator-(const Szam& other) const
	{
		return Szam(*this) -= other;
	}

	Szam& operator*=(const Szam& other)
	{
		const V newValue = value * other.value;
		error = (E) (other.error * error + abs(newValue) * eps + abs(other.value) * error + abs(value) * other.error);
		value = newValue;
		return *this;
	}

	Szam operator*(const Szam& other) const
	{
		return Szam(*this) *= other;
	}

	Szam operator/=(const Szam& other)
	{
		const V newValue = value / other.value;
		error = (E) ((error + abs(newValue) * other.error) / max(abs(other.value) - other.error, 0.0) + abs(newValue) * eps);
		value = newValue;
		return *this;
	}

	Szam operator/(const Szam& other) const
	{
		return Szam(*this) /= other;
	}

	V getValue() const
	{
		return value;
	}

	E getError() const
	{
		return error;
	}

	E getRelError() const
	{
		return (value == 0.0 && error == 0.0) ? 0 : error / abs(value);
	}

};

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::AbsErr, V, E> sqrt(const SZ<Method::AbsErr, V, E>& x)
{
	const V newValue = (V) sqrt(x.getValue());
	const E error = (E) (x.getError() * 0.5 / newValue + newValue * Szam<Method::AbsErr, V, E>::eps);
	return SZ<Method::AbsErr, V, E>(AbsoluteError(), newValue, error);
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::AbsErr, V, E> abs(const SZ<Method::AbsErr, V, E>& x)
{
	return SZ<Method::AbsErr, V, E>(AbsoluteError(), abs(x.getValue()), x.getError());
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::AbsErr, V, E> pow(const SZ<Method::AbsErr, V, E>& x, const SZ<Method::AbsErr, V, E>& y)
{
	//(a+alpha)^(b+beta)
	//beta * a^b * ln(a) + alpha * b * a ^ (b-1)
	const V value = (V) pow(x.getValue(), y.getValue());
	const E error = (E) (abs(value) * Szam<Method::AbsErr, V, E>::eps + abs(value) * y.getError() * abs(log(abs(x.getValue()))) + x.getError() * abs(y.getValue()) * abs(pow(x.getValue(), y.getValue() - 1)));
	return SZ<Method::AbsErr, V, E>(AbsoluteError(), value, error);
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::AbsErr, V, E> log(const SZ<Method::AbsErr, V, E>& x)
{
	const V value = (V) log(x.getValue());
	const E error = (E) (x.getError() / x.getValue() + abs(value) * Szam<Method::AbsErr, V, E>::eps);
	return SZ<Method::AbsErr, V, E>(AbsoluteError(), value, error);
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::AbsErr, V, E> cos(const SZ<Method::AbsErr, V, E>& x)
{
	const V value = (V) cos(x.getValue());
	const E error = (E) (x.getError() * abs(sin(x.getValue())) + abs(value) * Szam<Method::AbsErr, V, E>::eps);
	return SZ<Method::AbsErr, V, E>(AbsoluteError(), value, error);
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::AbsErr, V, E> sin(const SZ<Method::AbsErr, V, E>& x)
{
	const V value = (V) sin(x.getValue());
	const E error = (E) (x.getError() * abs(cos(x.getValue())) + abs(value) * Szam<Method::AbsErr, V, E>::eps);
	return SZ<Method::AbsErr, V, E>(AbsoluteError(), value, error);
}
