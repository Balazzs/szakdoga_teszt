#pragma once

#include <iostream>
#include <algorithm>
#include <math.h>
#include <qd/dd_real.h>
#include <qd/inline.h>
#include <limits>
#include <limits>
#include <type_traits>
#include <typeinfo>

using std::to_string;
using std::sqrt;
using std::pow;
using std::abs;
using std::log;
using std::cos;
using std::sin;
using std::max;
using std::floor;

enum Method {
	AbsErr,			//Overestimates the representation error (always takes epsilon)
	InverseCalc,	//No longer supported
					//Does the inverse of the last operation and tries to estimate the error based on the difference
	RepEst,			//(Precisely) calculates the error of the last operation
	ProbDist,		//No longer supported
					//A very naive probability distribution, assumes uniform error distribution on the first representation, 
					//after that only uses the representation error to widen the interval instead of calculating convolutions
	DblDouble,		//Calculates the results with 2 different precisions (for example double and double double)
					//then the error is the difference between them
	CENA			//AND HIS NAME IS JOHN... (Jokes aside:)
					//Takes the estimated errors with sign so they can cancel each other
					//https://hal.archives-ouvertes.fr/inria-00072615/document
					//not exactly the CENA method just something like that
					//almost the same as double precision
};

class AbsoluteError {};

template<Method M, class V, class E>
class Szam {
	//friend std::ostream& operator<<(std::ostream& os, Szam sz);

	V value;
	E error, relError;

public:

	Szam(V value = 0.0, E relError = std::numeric_limits<V>::epsilon())
	{
		this->value = value;
		this->relError = relError;
		this->error = relError*abs(value);
	}

	Szam(AbsoluteError dummy, V value = 0.0, E error = 0)
	{
		this->value = value;
		this->error = error;
		this->relError = error / abs(value);
	}

	Szam operator+(const Szam& other);

	Szam operator-(const Szam& other);

	Szam operator*(const Szam& other);

	Szam operator/(const Szam& other);

	V getValue() const
	{
		return value;
	}

	E getError() const
	{
		return error;
	}

	E getRelError() const
	{
		return relError;
	}
};

template<Method M, class V, class E>
std::ostream& operator<<(std::ostream& os, const Szam<M, V, E>& sz)
{
	return (os << sz.getValue() << "+/-" << sz.getError()  << " (" << sz.getRelError() << ")" );
}

template<Method M, class V, class E>
bool operator>(const Szam<M, V, E>& a, const Szam<M, V, E>& b)
{
	return a.getValue() > b.getValue();
}

template<Method M, class V, class E>
bool operator>=(const Szam<M, V, E>& a, const Szam<M, V, E>& b)
{
	return a.getValue() >= b.getValue();
}

template<Method M, class V, class E>
bool operator==(const Szam<M, V, E>& a, const Szam<M, V, E>& b)
{
	return a.getValue() == b.getValue() && a.getError() == b.getError();
}

template<Method M, class V, class E>
bool operator!=(const Szam<M, V, E>& a, const Szam<M, V, E>& b)
{
	return !(a == b);
}

template <class T>
double errorOf(T x)
{
	return 0;
}

template<template <Method, class, class> class SZ, Method M, class V, class E>
E errorOf(const SZ<M, V, E>& x)
{
	return x.getError();
}

template <class T>
double relErrorOf(T x)
{
	return 0;
}

template<template <Method, class, class> class SZ, Method M, class V, class E>
E relErrorOf(const SZ<M, V, E>& x)
{
	return x.getRelError();
}


template <class T>
double valueOf(T x)
{
	return 0;
}

template<template <Method, class, class> class SZ, Method M, class V, class E>
E valueOf(const SZ<M, V, E>& x)
{
	return x.getValue();
}

template<template <Method, class, class> class SZ, Method M, class V, class E>
bool operator<(const SZ<M, V, E>& x, const SZ<M, V, E>& y)
{
	return x.getValue() < y.getValue();
}

template<template <Method, class, class> class SZ, Method M, class V, class E>
bool operator>(const SZ<M, V, E>& x, const SZ<M, V, E>& y)
{
	return x.getValue() < y.getValue();
}

template<template <Method, class, class> class SZ, Method M, class V, class E, class D>
bool operator==(const SZ<M, V, E>& x, D y)
{
	return x.getValue() == y && x.getError() == 0.0;
}

template<template <Method, class, class> class SZ, Method M, class V, class E>
std::string to_string(const SZ<M, V, E>& x)
{
	return to_string(x.getValue()) + "+/-" + to_string(x.getError()) + " (" + to_string(x.getRelError()) + ")";
}



template<template <Method, class, class> class SZ, Method M, class V, class E>
std::string getSzamName(const SZ<M, V, E>& dummy)
{
	std::string methodName;
	switch (M)
	{
	case AbsErr:
		methodName = "AbsErr";
		break;
	case RepEst:
		methodName = "RepEst";
		break;
	case CENA:
		methodName = "CENA";
		break;
	case DblDouble:
		methodName = "DblDouble";
		break;
	}

	return methodName + "_" + typeid(V).name() + "_" + typeid(E).name();
}