#pragma once

#include "Szam.h"

struct v2Dummy {};

template<class V, class E>
class Szam<Method::DblDouble, V, E> {

	V value;
	E value2;

public:

	static constexpr V eps = std::numeric_limits<V>::epsilon();
	static constexpr int digits = std::numeric_limits<V>::digits;

	Szam()
	{
		this->value = (V) 0.0;
		this->value2 = (E) 0.0;
	}

	template <class V_, typename = std::enable_if_t< std::is_arithmetic< V_ >::value > >
	Szam(V_ value)
	{
		this->value = (V) value;
		this->value2 = (E) value;
	}

	template <class V_, typename = std::enable_if_t< std::is_arithmetic< V_ >::value > >
	Szam(V_ value, E relErr)
	{
		this->value = (V) value;
		this->value2 = (E) (value + value * relErr);
	}

	Szam(v2Dummy dummy)
	{
		this->value = V(0.0);
		this->value2 = E(0.0);
	}

	template <class V_, typename = std::enable_if_t< std::is_arithmetic< V_ >::value > >
	Szam(v2Dummy dummy, V_ value, E value2 = E(0.0))
	{
		this->value = (V) value;
		this->value2 = value2;
	}

	Szam(AbsoluteError dummy)
	{
		this->value = V(0.0);
		this->value2 = E(0.0);
	}

	template <class V_, typename = std::enable_if_t< std::is_arithmetic< V_ >::value > >
	Szam(AbsoluteError dummy, V_ value, E error = E(0.0))
	{
		this->value = (V) value;
		this->value2 = (E) (value + error);
	}

	Szam& operator+=(const Szam& other)
	{
		value = value + other.value;
		value2 = value2 + other.value2;
		return *this;
	}

	Szam operator+(const Szam& other) const
	{
		return Szam(*this) += other;
	}

	Szam operator-()
	{
		return Szam(v2Dummy(), -value, -value2);
	}

	Szam& operator-=(const Szam& other)
	{
		value = value - other.value;
		value2 = value2 - other.value2;
		return *this;
	}

	Szam operator-(const Szam& other) const
	{
		return Szam(*this) -= other;
	}

	Szam& operator*=(const Szam& other)
	{
		value = value * other.value;
		value2 = value2 * other.value2;
		return *this;
	}

	Szam operator*(const Szam& other) const
	{
		return Szam(*this) *= other;
	}

	Szam operator/=(const Szam& other)
	{
		value = value / other.value;
		value2 = value2 / other.value2;
		return *this;
	}

	Szam operator/(const Szam& other) const
	{
		return Szam(*this) /= other;
	}

	V getValue() const
	{
		return value;
	}


	E getValue2() const
	{
		return value2;
	}

	E getError() const
	{
		return (E) (value2 - value);
	}

	E getRelError() const
	{
		return (E) (getError() / value);
	}

};

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::DblDouble, V, E> sqrt(const SZ<Method::DblDouble, V, E>& x)
{
	return SZ<Method::DblDouble, V, E>(v2Dummy(), (V) sqrt(x.getValue()), (E) sqrt(x.getValue2()));
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::DblDouble, V, E> abs(const SZ<Method::DblDouble, V, E>& x)
{
	return SZ<Method::DblDouble, V, E>(v2Dummy(), (V) abs(x.getValue()), (E) abs(x.getValue2()));
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::DblDouble, V, E> pow(const SZ<Method::DblDouble, V, E>& x, const SZ<Method::DblDouble, V, E>& y)
{
	return SZ<Method::DblDouble, V, E>(v2Dummy(), (V) pow(x.getValue(), y.getValue()), (E) pow(x.getValue2(), y.getValue2()) );
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::DblDouble, V, E> log(const SZ<Method::DblDouble, V, E>& x)
{
	return SZ<Method::DblDouble, V, E>(v2Dummy(), (V) log(x.getValue()), (E) log(x.getValue2()));
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::DblDouble, V, E> cos(const SZ<Method::DblDouble, V, E>& x)
{
	return SZ<Method::DblDouble, V, E>(v2Dummy(), (V) cos(x.getValue()), (E) cos(x.getValue2()));
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::DblDouble, V, E> sin(const SZ<Method::DblDouble, V, E>& x)
{
	return SZ<Method::DblDouble, V, E>(v2Dummy(), (V) sin(x.getValue()), (E) sin(x.getValue2()));
}