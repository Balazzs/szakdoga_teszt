#pragma once

#include <vector>
#include <functional>
#include <numeric>
#include <assert.h>
#include <algorithm>
#include <stack>
#include <fstream>

#include <set>

#include "Scope.hpp"
//#include "algorithms.h"
#include "integrals.hpp"

using std::to_string;


template <class T>
AST_<T> sqrt(const AST_<T>& a);

template <class T>
AST_<T> pow(const AST_<T>& a, const AST_<T>& b);

template <class T>
AST_<T> log(const AST_<T>& a);

template <class T>
AST_<T> sin(const AST_<T>& a);

template <class T>
AST_<T> cos(const AST_<T>& a);


enum Muvelet
{
	Constant,
	Variable,
	Plus,
	Minus,
	UnaryMinus,
	Mul,
	Div,
	Sqrt,
	Pow,
	Ln,
	Sin,
	Cos,
	
	//Integral; f(x); x0; x1; dx;
	Integrate,
	
	//Is a collection of Defs applied to an expression
	Binding,
	
	//An application of an expression as a Variable
	//So basically the definition of the Variable
	Def,
};

//For the definite Integral
enum IntLeg : size_t
{
	IntAST	= 0,
	IntVar  = 1,
	IntX0	= 2,
	IntX1	= 3,
	IntdX	= 4
};

template <class T>
struct VisuInfo
{
	friend AST_<T>;

	double colErrFloor = 0, colErrCeil = 500;
	bool logarithmic = true;

	bool calculateValues = false;
	
	std::string fname;
	std::ofstream fstream;

	std::function< double(const T&)> valF = [](const T& t) {return 0; };
	std::function< std::string (const T&)> strF = [](const T& t) {return "";};

public:

	VisuInfo(const std::string& fname, bool calculateValues, bool logarithmic, const std::function< double(const T&)>& valF, const std::function< std::string (const T&)>& strF) : logarithmic(logarithmic), calculateValues(calculateValues), fname(fname), valF(valF), strF(strF)
	{ }
	
	VisuInfo(const std::string& fname = "out.viz") : fname(fname)
	{ }

	std::string getColor(double val)
	{
		if (std::isnan(val))
			return "0 0 0.5";

		double mult = 120.0 / 360.0;
		//TODO
		double relative = (val - colErrFloor) / (colErrCeil - colErrFloor);

		if (logarithmic)
			relative = std::log10(relative) + mult;
		else
			relative = relative * mult;

		std::string color = "";

		color += std::to_string( mult - std::max(std::min(relative, mult), 0.0)) + " 1.0 1.0";
		
		return color;
	}
	
	static std::string getPointInfo(const AST_<T>& p)
	{
		std::string mstr;

		switch (p.m)
		{
		case Binding:
			mstr = "Bind";
			break;
		case Constant:
			mstr = "Const";
			break;
		case Cos:
			mstr = "cos";
			break;
		case Def:
			mstr = "Def";
			mstr += "\\n" + p.name;
			break;
		case Div:
			mstr = "/";
			break;
		case Integrate:
			mstr = "Int";
			break;
		case Ln:
			mstr = "Ln";
			break;
		case Minus:
			mstr = "-";
			break;
		case Mul:
			mstr = "x";
			break;
		case Plus:
			mstr = "+";
			break;
		case Pow:
			mstr = "^";
			break;
		case Sin:
			mstr = "sin";
			break;
		case Sqrt:
			mstr = "sqrt";
			break;
		case UnaryMinus:
			mstr = "-";
			break;
		case Variable:
			mstr = "Var";
			mstr += "\\n" + p.name;
			break;
		default:
			mstr = "";
		}

		return mstr;
	}

	void connect(int from, int to)
	{
		fstream << from << " -> " << to << "\n";
	}

	void writeValue(const AST_<T>& ast, int point)
	{
		fstream << point <<  " [ style=filled color=\""  << getColor(valF(ast.value)) << "\" label = \""  << getPointInfo(ast) << strF(ast.value) << "\" ]" << "\n";
	}

	void open()
	{
		open(fname);
	}

	void open(const std::string& filename)
	{
		fname = filename;
		fstream.open(fname);
		fstream << "digraph {" << std::endl;
	}

	void close()
	{
		fstream << "}";
		fstream.close();
	}
};

template <class T>
class AST_
{
	std::string name;			//For variables
	T value;					//For constants
	T* ptr = nullptr;		//For variables
	Muvelet m;
	
	std::vector< AST_ > legs;

	//Friends
	friend VisuInfo<T>;

	friend bool unit_tests();

	friend AST_ sqrt<>(const AST_& a);
	friend AST_ pow<>(const AST_& a, const AST_& b);
	friend AST_ log<>(const AST_& a);
	friend AST_ sin<>(const AST_& a);
	friend AST_ cos<>(const AST_& a);

	template <class U>
	friend class AST_;

private:
	AST_(Muvelet m)
	{
		this->m = m;
	}

public:

	//Constructors with Muvelet and ASTs//////
	//////////////////////////////////////////

	AST_( Muvelet m, const std::vector<AST_>& vec)
	{
		this->m = m;
		this->legs = vec;
	}
	
	AST_( Muvelet m, std::vector<AST_>&& vec)
	{
		this->m = m;
		this->legs = std::move(vec);
	}
	
	template <typename... ASTs>
	AST_( Muvelet m, ASTs&&... asts)
	{
		//TODO static check whether ASTs... is AST
		//and use static_assert, not necessary but nice
		
		*this = AST_(m, {asts...});
	}
	
	//Copy/move constructors andd assignment//
	//////////////////////////////////////////
	AST_(const AST_& other)
	{
		this->m = other.m;
		this->name = other.name;
		this->ptr = other.ptr;
		this->value = other.value;
		this->legs = other.legs;
	}
	
	AST_(AST_&& other)
	{
		this->m = other.m;
		this->ptr = other.ptr;
		this->value = other.value;
		this->name = std::move(other.name);
		this->legs = std::move(other.legs);
	}
	
	AST_& operator=(const AST_& other)
	{
		this->m = other.m;
		this->name = other.name;
		this->ptr = other.ptr;
		this->value = other.value;

		//Is the last one so we can't reset other by copying over it if it's a child node of this 
		this->legs = other.legs;

		return *this;
	}
	
	AST_& operator=(AST_&& other)
	{
		this->m = other.m;
		this->name = std::move(other.name);
		this->ptr = other.ptr;
		this->value = other.value;

		this->legs = std::move(other.legs);
		
		return *this;
	}
	
	//Operators///////////////////////////////
	//////////////////////////////////////////
	
	AST_ operator+(const AST_& other)
	{
		return AST_(Plus, *this, other);
	}

	AST_& operator+=(const AST_& other)
	{
		*this = operator+(other);
		return *this;
	}

	AST_ operator-()
	{
		return AST_(UnaryMinus, *this);
	}

	AST_ operator-(const AST_& other)
	{
		return AST_(Minus, *this, other);
	}

	AST_& operator-=(const AST_& other)
	{
		*this = operator-(*this, other);
		return *this;
	}

	AST_ operator*(const AST_& other)
	{
		return AST_(Mul, *this, other);
	}

	AST_& operator*=(const AST_& other)
	{
		*this = operator*(*this, other);
		return *this;
	}

	AST_ operator/(const AST_& other)
	{
		return AST_(Div, *this, other);
	}

	AST_& operator/=(const AST_& other)
	{
		*this = operator/(*this, other);
		return *this;
	}

	
	//Integration/////////////////////////////
	//////////////////////////////////////////
	
	AST_ integral(const std::string& xVar, const AST_& x0, const AST_& x1, const AST_& dx)
	{
		return AST_(Integrate, *this, AST_(xVar), x0, x1, dx).boundI( {xVar} );
	}

	AST_& integrate(const std::string& xVar, const AST_& x0, const AST_& x1, const AST_& dx)
	{
		return operator=(integral(xVar, x0, x1, dx));
	}
	
	
	//Constant////////////////////////////////
	//////////////////////////////////////////
	
	AST_(const T& value)
	{
		this->m = Constant;
		this->value = value;
	}
	
	AST_(T&& value)
	{
		this->m = Constant;
		this->value = std::move(value);
	}
	
	//Variable////////////////////////////////
	//////////////////////////////////////////
	
private:
	AST_(){}

public:
	
	//Variable
	AST_(const std::string& name)
	{
		this->m = Variable;
		this->name = name;
	}
	
public:	
	//The expression is simply a variable with name <name>
	//and it has the <ast> bound to <name>
	AST_(const std::string& name, const AST_& ast)
	{
		operator=(AST_(name).bound( {{name, ast}} ));
	}
	
	AST_(const std::string& name, AST_&& ast)
	{
		operator=(AST_(name).bound( {{name, std::move(ast)}} ));
	}
	
	AST_ nameAs(const std::string& name)
	{
		return AST_(name, *this);
	}
	
private:
	//Creates an Def node from the name-ASTvalue pair / name and ASTvalue
	static AST_ getDef(const std::string& name, const AST_& value)
	{
		AST_ ret = AST_(Def, value);
		ret.name = name;
		return ret;
	}
		
	static AST_ getDef(const std::string& name, AST_&& value)
	{
		AST_ ret = AST_(Def, std::move(value));
		ret.name = name;
		return ret;
	}
	
	static AST_ getIntegralDef(const std::string& name)
	{
		AST_ ret = AST_(name);
		ret.m = Def;
		return ret;
	}

public:	
	
	AST_& bind( const std::vector< std::pair< std::string, T> >& pairs)
	{
		return operator=( bound(pairs) );
	}
	
	AST_& bind( const std::vector< std::pair< std::string, AST_> >& pairs)
	{
		return operator=( bound(pairs) );	
	}
	
	AST_ bound( const std::vector< std::pair< std::string, AST_> >& pairs)
	{
		std::vector<AST_> legs;
		
		//The first one is the expression
		legs.push_back(*this);
		
		//For each bound variable make a new Def leg 
		for(auto& p : pairs)
		{
			legs.push_back( getDef(p.first, p.second ) );
		}
		
		return AST_(Binding, legs );
	}
	
	AST_ bound( std::vector< std::pair< std::string, AST_> >&& pairs)
	{
		std::vector<AST_> legs;
		
		//The first one is the expression
		legs.push_back(*this);
		
		//For each bound variable make a new Def leg 
		for(auto&& p : pairs)
		{
			legs.push_back( getDef(p.first, std::move(p.second) ) );
		}
		
		return AST_(Binding, legs );
	}
	
	//Binds integral variables, they don't have values
	AST_ boundI( const std::vector<std::string>& vars)
	{
		std::vector<AST_> legs;
		
		//The first one is the expression
		legs.push_back(*this);
		
		//For each bound variable make a new Def leg 
		for(auto& v : vars)
			legs.push_back( getIntegralDef(v) );
		
		return AST_(Binding, legs );
	}
	
public:
	
	bool setLinks()
	{
		return setLinks(Scope<T>());
	}
	
	bool setLinks(const Scope<T>& scope)
	{
		switch(m)
		{
			case Variable:
			{
				ptr = scope.get(name);
				return ptr != nullptr;
			}
			case Binding:
			{
				//First set the links of the Defs
				for (size_t i = 1; i < legs.size(); i++)
					legs[i].setLinks(scope);

				//Add the new Defs to the Scope
				Scope<T> newScope(scope);
				
				for( size_t i = 1; i < legs.size(); i++)
					newScope.add(legs[i].name ,&legs[i].value);
				
				//Set links of inner AST
				return legs[0].setLinks(newScope);
			}
			default:
			{
				bool isOK = true;
				for(auto& ast : legs)
					isOK |= ast.setLinks(scope);
				
				return isOK;
			}
		}
		
	}
	
	T evaluate()
	{
		switch (m)
		{
		case Constant:
			return value;
		case Variable:
			return *ptr;
		case Plus:
			return std::accumulate(legs.begin() + 1, legs.end(), legs[0].evaluate(), [&](const T& a, AST_& b) {return a + b.evaluate(); });
		case Minus:
			return legs[0].evaluate() - legs[1].evaluate();
		case UnaryMinus:
			return -legs[0].evaluate();
		case Mul:
			return std::accumulate(legs.begin() + 1, legs.end(), legs[0].evaluate(), [&](const T& a, AST_& b) {return a * b.evaluate(); });
		case Div:
			return legs[0].evaluate() / legs[1].evaluate();
		case Sqrt:
			return sqrt(legs[0].evaluate());
		case Pow:
			return pow(legs[0].evaluate(), legs[1].evaluate());
		case Ln:
			return log(legs[0].evaluate());
		case Sin:
			return sin(legs[0].evaluate());
		case Cos:
			return cos(legs[0].evaluate());
		case Integrate:
		{
			//We need to change the variable before each evaluation
			auto reeval = [&](const T& x)
			{
				(*(legs[IntVar].ptr)) = x;
				return legs[IntAST].evaluate();
			};

			//Integrate
			T ret = boole_integrate<T>({ legs[IntX0].evaluate(), legs[IntX1].evaluate() }, legs[IntdX].evaluate(), reeval);
			
			return ret;
		}
		case Binding:
		{
			//First calculate the bindings
			for( size_t i = 1; i < legs.size(); i++)
				legs[i].evaluate();
			
			//Then the expression
			return legs[0].evaluate();
		}
		case Def:
		{
			//Set the stored value if it's not an Integral variable
			if(legs.size())
				value = legs[0].evaluate();
			return T(NAN);
		}
		default:
			assert(false);
			return T(NAN);
		}
		
	}
	
	//Evaluates and stores the result in value
	T& evaluateToMemory()
	{
		switch (m)
		{
		case Constant:
			return value = value;
		case Variable:
			return value = *ptr;
		case Plus:
			return value = std::accumulate(legs.begin() + 1, legs.end(), legs[0].evaluateToMemory(), [&](const T& a, AST_& b) {return a + b.evaluateToMemory(); });
		case Minus:
			return value = legs[0].evaluateToMemory() - legs[1].evaluateToMemory();
		case UnaryMinus:
			return value = -legs[0].evaluateToMemory();
		case Mul:
			return value = std::accumulate(legs.begin() + 1, legs.end(), legs[0].evaluateToMemory(), [&](const T& a, AST_& b) {return a * b.evaluateToMemory(); });
		case Div:
			return value = legs[0].evaluateToMemory() / legs[1].evaluateToMemory();
		case Sqrt:
			return value = sqrt(legs[0].evaluateToMemory());
		case Pow:
			return value = pow(legs[0].evaluateToMemory(), legs[1].evaluateToMemory());
		case Ln:
			return value = log(legs[0].evaluateToMemory());
		case Sin:
			return value = sin(legs[0].evaluateToMemory());
		case Cos:
			return value = cos(legs[0].evaluateToMemory());
		case Integrate:
		{
			//We need to change the variable before each evaluation
			auto reeval = [&](const T& x)
			{
				(*(legs[IntVar].ptr)) = x;
				return legs[IntAST].evaluateToMemory();
			};

			//Integrate
			T ret = boole_integrate<T>({ legs[IntX0].evaluateToMemory(), legs[IntX1].evaluateToMemory() }, legs[IntdX].evaluateToMemory(), reeval);
			
			return value = ret;
		}
		case Binding:
		{
			//First calculate the bindings
			for( size_t i = 1; i < legs.size(); i++)
				legs[i].evaluateToMemory();
			
			//Then the expression
			return value = legs[0].evaluateToMemory();
		}
		case Def:
		{
			//Set the stored value if it's not an Integral variable
			if(legs.size())
				return value = legs[0].evaluateToMemory();
			else
				return value = T(NAN);
		}
		default:
			assert(false);
			return value = T(NAN);
		}
		
	}

	void visualize(VisuInfo<T>& viz, int& currID, int ID) const
	{
		viz.writeValue( *this, ID);
		
		for(const AST_& ast : legs)
		{
			const int childID = ++currID;
			viz.connect(ID, childID);
			ast.visualize(viz, currID, childID);
		}
	}


	T evaluate(const Scope<T>& scope)
	{
		setLinks(scope);
		return evaluate();
	}
	
	AST_& simplify()
	{
		return operator=(simplified({}).first);
	}
	
	AST_ simplified()
	{
		return simplified({}).first;
	}

private:
	
	class bindingIter : public std::iterator<std::forward_iterator_tag, AST_ >
	{
		AST_& me;
		size_t legInd;
		
		bool isEnd() const
		{
			return me.legs.size() <= legInd;
		}

		bool hasBinding() const
		{
			auto& L = me.legs[legInd];
			return L.m == Binding || (L.m == Def && L.legs.size() && L.legs[0].m == Binding);
		}
		
	public:
		//subindex is always >0 since 0 is the AST itself in a Binding
		//  so we can init subIndexFrom to 0 and start a search for the next Binding
		bindingIter(AST_& ast, size_t legindexFrom = 0)
			: me(ast), legInd(legindexFrom)
		{
			//find the next one if it's not a Binding already
			if(!isEnd() && !hasBinding())
				operator++();
		}

		bindingIter(const bindingIter& other)
			: me(other.me), legInd(other.legInd) {}
		
		bindingIter& operator++()
		{
			//Go forward until we reach end() or find a Binding
			do {
				legInd++;
			} while (!isEnd() && !hasBinding());

			return *this;
		}
		
		bindingIter operator++(int)
		{ bindingIter retval = *this; ++(*this); return retval; }
		
		bool operator==(bindingIter other) const
		{ return (legInd >= me.legs.size() && other.legInd >= me.legs.size()) || (legInd == other.legInd); }
		
		bool operator!=(bindingIter other) const
		{ return !(*this == other); }

		AST_& operator*() const
		{
			if (me.legs[legInd].m == Def)
				return me.legs[legInd].legs[0];
			else
				return me.legs[legInd];
		}
		
		bool isDef() const
		{
			return me.legs[legInd].m == Def;
		}
	};

	bindingIter bindingBegin()
	{
		return bindingIter(*this);
	}
	
	bindingIter bindingEnd()
	{
		return bindingIter(*this, legs.size());
	}

public:

	std::set<std::string> getLambdas(const std::set<std::string>& defs) const
	{
		std::set<std::string> lambdas;

		if (m == Binding)
		{
			std::set<std::string> newDefs(defs);

			for (size_t i = 1; i < legs.size(); i++)
			{
				auto p = legs[i].getLambdas(defs);
				lambdas.insert(p.begin(), p.end());

				newDefs.insert(legs[i].name);
			}

			auto leg0 = legs[0].getLambdas(newDefs);

			lambdas.insert(leg0.begin(), leg0.end());

			return lambdas;
		}

		//Defs themselves won't become Constants
		if (m == Def)
		{
			if (!legs.size())
				return { };
			else
				return legs[0].getLambdas(defs);
		}

		//Shouldn't simplify all legs of an integration
		//Since it's kind of like a Context with 1 Var Def
		if (m == Integrate)
		{
			auto p1 = legs[IntX0].getLambdas(defs);
			auto p2 = legs[IntX1].getLambdas(defs);
			auto p3 = legs[IntdX].getLambdas(defs);

			auto p0 = legs[IntAST].getLambdas(defs);

			lambdas = p1;
			lambdas.insert(p2.begin(), p2.end());
			lambdas.insert(p3.begin(), p3.end());
			lambdas.insert(p0.begin(), p0.end());

			return lambdas;
		}
		
		if (m == Variable)
		{
			auto find = defs.find(name);

			if (find != defs.end())
				return {};
			else
				return { name };
		}

		////GENERAL

		//Simplify the legs
		for (auto& x : legs)
		{
			auto p = x.getLambdas(defs);
			lambdas.insert(p.begin(), p.end());
		}
		
		return lambdas;

	}

private:

	//Handles the given Def based on the list of currently moveable / non-moveable Defs
	void tryDef(std::map<std::string, AST_*>& mightMerge, std::set<std::string>& stays, AST_& app, const std::set<std::string>& upperDefs)
	{
		auto findIt = mightMerge.find(app.name);

		//If a Def with the same name might bubble up
		if(findIt != mightMerge.end())
		{
			//If they are the same everything's fine
			if( *(findIt->second) == app)
			{
				//mightMerge.insert({app.name, &app});
			}
			else
			{
				mightMerge.erase(findIt);
				stays.insert(app.name);
			}
		}
		else
		{
			//It might be blocked already
			bool canMove = stays.find(app.name) == stays.end();
			
			//It might become blocked by an upper Def:
			//Maybe it has a free param that is set in the Binding
			// right above it so it can't move up
			auto lambdasofApp = app.getLambdas({});

			canMove &= std::none_of(lambdasofApp.begin(), lambdasofApp.end(), [&upperDefs](const auto& lambda) { return upperDefs.find(lambda) != upperDefs.end(); });

			//If it isn't there then it can bubble up
			if( canMove )
				mightMerge.insert({app.name, &app});
		}
	}
	
	//Should not rebuild, just modify inplace
	//TODO change inner state or return a new AST do not do both
	AST_ bindingMerge(const std::map<std::string, AST_*>& defs, const std::set<std::string>& lambdas)
	{
		//The Defs that might go up above their current level
		std::map<std::string, AST_*> mightMerge;

		//The Defs that gotta stay where they are
		//If the current AST is a function/lambda of some variable
		//  then Bindings with the same name can't move up
		std::set<std::string> stays = lambdas;
		
		//The Defs in the upper Binding
		//If any Def in the lower Bindings has a lambda param
		// with the same name it can't move up
		std::set<std::string> upperDefs;

		//Integral variables and it's dependencies can't escape
		// they have to be reevaluated each time
		if(m == Integrate)
			upperDefs.insert( legs[IntVar].name );

		//The Defs that should move directly above the Current Binding
		//since they come from the inner Defs and need to be applied earlier
		std::set<std::string> aboveDefs;

		if (m == Binding)
		{
			

			for (size_t ind = 1; ind < legs.size(); ind++)
			{
				upperDefs.insert(legs[ind].name);

				tryDef(mightMerge, stays, legs[ind], {});
			}

		}

		for(bindingIter iter = bindingBegin(); iter != bindingEnd(); iter++)
			for (size_t i = 1; i < (*iter).legs.size(); i++)
			{
				AST_& app = (*iter).legs[i];
				tryDef(mightMerge, stays, app, upperDefs);

				//Goes above the current Binding level
				if (iter.isDef())
					aboveDefs.insert(app.name);
			}
			
		//If there are 0 ones just return as we are
		if (mightMerge.size() == 0)
			return AST_(*this);

		//Collect the Defs into a new vector
		std::vector<AST_> bubbleDefs;

		for (auto it = mightMerge.begin(); it != mightMerge.end(); it++)
		{
			auto ASTptr = it->second;
			bubbleDefs.push_back(AST_(*ASTptr));//cant std::move this one
		}


		//TODO remove any AST which is the same as one in the Scope
		//  since they are meaningless
		//  this should not exist, i mean no path should have 2 Defs of the same name
		

		
		//Delete the moveable ones from below
		//Check for empty Bindinsg

		//Also check whether we managed to actually move (/delete) something
		bool didSomething = false;

		//When removing the movable ones pay attention to the Bindings inside Defs
		for (bindingIter iter = bindingBegin(); iter != bindingEnd(); iter++)
		{
			AST_& bind = *iter;

			//Start from index 1
			bind.legs.erase(std::remove_if(++bind.legs.begin(), bind.legs.end(), \
				[&mightMerge, &didSomething](const AST_& app) {
				bool deleteMe = (mightMerge.find(app.name) != mightMerge.end()) ; 
				didSomething |= deleteMe;
				return deleteMe;
			}), bind.legs.end() );

			if (bind.legs.size() == 1)
				bind = AST_(bind.legs[0]);
		}
		
		//If we didn't do anything
		if (!didSomething)
			return AST_(*this);

		

		//Return with new common Binding (if there were Defs to bubble up)
		if(m == Binding)
		{
			//Remove every Def that is already in the Binding
			//NOTE this case should not hapen to exist??
			bubbleDefs.erase(std::remove_if ( bubbleDefs.begin(), bubbleDefs.end(), \
				[&](const AST_& def) {return std::any_of(++legs.begin(), legs.end(), [&def](const AST_& leg) {return leg.name == def.name; });  }), bubbleDefs.end());
			
			std::vector<AST_> aboveASTs;

			//Inserts the new legs that don't go above but go into the Binding into legs
			//  and the ones that go into an upper Binding into aboveASTs
			std::partition_copy(bubbleDefs.begin(), bubbleDefs.end(), std::back_inserter(legs), std::back_inserter(aboveASTs), \
				[&aboveDefs](const AST_& ast) { return aboveDefs.find(ast.name) == aboveDefs.end(); });

			if (aboveASTs.size())
			{
				aboveASTs.insert(aboveASTs.begin(), AST_(*this).bindingMerge(defs, lambdas) );
				return AST_(Binding, aboveASTs).bindingMerge(defs, lambdas);
			}
			else
				return AST_(*this).bindingMerge(defs, lambdas);
		}
		else
		{
			bubbleDefs.insert(bubbleDefs.begin(), AST_(*this).bindingMerge(defs, lambdas));
			
			//Create AST from the legs
			return AST_(Binding, std::move(bubbleDefs)).bindingMerge(defs, lambdas);
		}
	}
	
	Scope<T> defsToScope(const std::map<std::string, AST_*> defs)
	{
		Scope<T> scope;
		for(auto& it : defs)
			scope.add(it.first, &it.second->value);
		
		return scope;
	}
	
	std::pair<AST_, std::set<std::string> > simplified(const std::map<std::string, AST_*>& defs)
	{
		//Bindings simplify their Defs before simplifying their AST
		//Also they add the Constant Variables to the Scope
		if (m == Binding)
		{
			std::map<std::string, AST_*> newDefs(defs);
			std::vector<AST_> newLegs;
			
			//Don't resize it until 
			newLegs.reserve(legs.size());

			std::set<std::string> lambdaVars;
			std::set<std::string> apps;
			
			

			for(size_t i = 1; i < legs.size(); i++)
			{
				//Get the simplified leg
				auto pair = legs[i].simplified(defs);
				
				//Add the variables
				lambdaVars.insert(pair.second.begin(), pair.second.end());
			
				//Shadow the outer Def
				//newScope.remove(app.name);
				
				//Add to appnames
				apps.insert(pair.first.name);
				
				//Add the Def to the new legs
				//Even the Constant ones go to the AST temporarily
				//  since we need their values for a short time
				newLegs.push_back(std::move(pair.first));
				
				//If it's Constant add to the new defs
				//if(app.legs.size() && app.legs[0].m == Constant)

				//These pointers only need to be valid until used for legs[0]'s simplification
				newDefs[newLegs.back().name] = &(newLegs.back());
			}
			
			//Simplify the AST with the new defs
			auto retZero = legs[0].simplified(newDefs);
			
			//The lambda params of this Binding are the lambda params from the Defs
			//  and the params which can go through the bindings from the AST leg (0)
			std::set<std::string> bubbleMeUp, retVars;
			
			std::set_difference(retZero.second.begin(), retZero.second.end(), apps.begin(), apps.end(), std::inserter(bubbleMeUp, bubbleMeUp.begin()) );
			
			std::set_union(lambdaVars.begin(), lambdaVars.end(), bubbleMeUp.begin(), bubbleMeUp.end(), std::inserter(retVars, retVars.begin()));
			
			//Now let's remove the Constants from the Binding
			//Also if 0 legs remain then destroy the binding alltogether
			newLegs.erase(std::remove_if(newLegs.begin(), newLegs.end(), [](const AST_& app){return app.legs.size() && app.legs[0].m == Constant;}), newLegs.end());
			
			if(newLegs.size())
			{
				newLegs.insert(newLegs.begin(), std::move(retZero.first) );
				return { AST_(Binding, newLegs).bindingMerge(defs, retVars) , retVars};
			}
			else
				return retZero;
		}
		
		//Defs themselves won't become Constants
		if (m == Def)
		{
			if(!legs.size())
				return { AST_(*this) , { } };
			
			auto pair = legs[0].simplified(defs);
			
			//Definitions don't move their Bindings above themselves
			return { getDef(name, pair.first), pair.second};
		}

		//Shouldn't simplify all legs of an integration
		//Since it's kind of like a Context with 1 Var Def
		if (m == Integrate)
		{			
			auto p1 = legs[IntX0].simplified(defs);
			auto p2 = legs[IntX1].simplified(defs);
			auto p3 = legs[IntdX].simplified(defs);
			
			auto p0 = legs[IntAST].simplified(defs);
			
			std::set<std::string> lambdas = p1.second;
			lambdas.insert(p2.second.begin(), p2.second.end());
			lambdas.insert(p3.second.begin(), p3.second.end());
			lambdas.insert(p0.second.begin(), p0.second.end());

			if (p0.first.m == Constant && p1.first.m == Constant && p2.first.m == Constant && p3.first.m == Constant)//NOTE not really like this
				return { AST_(Integrate, p0.first, legs[1], p1.first, p2.first, p3.first).evaluate(defsToScope(defs)) , {} };
			else
				return { AST_(Integrate, p0.first, legs[1], p1.first, p2.first, p3.first).bindingMerge(defs, lambdas), lambdas };
		}
		
		
		////GENERAL
		
		std::vector<AST_> newLegs;
		std::set<std::string> lambdas;
		
		//Simplify the legs
		for (auto& x : legs)
		{
			auto p = x.simplified(defs);
			newLegs.push_back(std::move(p.first));
			lambdas.insert(p.second.begin(), p.second.end());
		}

		//Check whether the whole thing is Constant
		bool allConst = newLegs.size() != 0;
		for (auto& x : newLegs)
			allConst &= x.m == Constant;
		
		//If it is then just become the Constant you are
		if (allConst)
			return {AST_(m , newLegs).evaluate(), {} };

		//Otherwise try to simplify it; check for some basic cases
		switch (m)
		{
		case Integrate:
		case Binding:
		case Def:
		case UnaryMinus:
		case Constant:
			//Do nothing
			break;
		case Variable:
		{
			auto find = defs.find(name);
			if (find != defs.end() && find->second->legs.size() && find->second->legs[0].m == Constant)
				return { AST_(find->second->legs[0]), {} };
			else
				return { *this, {name} };
			break;
		}
		case Plus:
		{
			//Find the first non-Constant
			auto nonConstIt = std::find_if_not(newLegs.begin(), newLegs.end(), [](const AST_& ast) { return ast.m == Constant; });

			//If we have at least 1 Const
			if (nonConstIt != newLegs.begin())
			{
				//From the beginning to this one sum all the Constant ASTs
				auto sum = std::accumulate(newLegs.begin() + 1, nonConstIt, newLegs[0].evaluate(), [&](const T& a, AST_& b) {return a + b.value; });

				//Remove the Constant elements
				newLegs.erase(newLegs.begin(), nonConstIt);

				//Insert the sum
				newLegs.insert(newLegs.begin(), AST_(sum));
			}
			break;
		}
		case Minus:
		{
			//x - 0 => x
			if (newLegs[1].m == Constant && newLegs[1].value == 0.0)
			{
				return { newLegs[0], lambdas };
			}
			break;
		}
		case Mul:
		{
			//Find the first non-Constant
			auto nonConstIt = std::find_if_not(newLegs.begin(), newLegs.end(), [](const AST_& ast) { return ast.m == Constant; });

			//If we have at least 1 Const
			if (nonConstIt != newLegs.begin())
			{
				//From the beginning to this one take the product of all the Constant ASTs
				auto prod = std::accumulate(newLegs.begin() + 1, nonConstIt, newLegs[0].value, [&](const T& a, AST_& b) {return a * b.value; });

				//Remove the Constant elements
				newLegs.erase(newLegs.begin(), nonConstIt);

				//Insert the product
				newLegs.insert(newLegs.begin(), AST_(prod));
			}
			break;
		}
		case Div:
		{
			// 0 / x => 0
			if (newLegs[0].m == Constant && newLegs[0].value == 0.0)
			{
				return {AST_(0.0), {} };
			}
			// x / 1 => x
			if (newLegs[1].m == Constant && newLegs[1].value == 1.0)
			{
				return { newLegs[0], lambdas};
			}
			break;
		}
		case Sqrt:
			break;
		case Pow:
		{
			//0 ^ x => 0
			if (newLegs[0].m == Constant && newLegs[0].value == 0.0)
				return { AST_(0.0), {} };
			//1 ^ x => 1
			if (newLegs[0].m == Constant && newLegs[0].value == 1.0)
				return { AST_(1.0), {} };
			//x ^ 0 => 1
			if (newLegs[1].m == Constant && newLegs[1].value == 0.0)
				return { AST_(1.0), {} };
			//x ^ 1 => x
			if (newLegs[1].m == Constant && newLegs[1].value == 1.0)
				return { newLegs[1], lambdas };
			
			break;
		}
		case Ln:
			break;
		case Sin:
			break;
		case Cos:
			break;
		}

		AST_ ret = AST_(m, newLegs);
		ret.name = name;
		ret.value = value;
		ret.ptr = nullptr;
		
		return {ret.bindingMerge(defs, lambdas), lambdas};
		
	}

public:

	template <class U, bool castAllValues = false>
	AST_<U> recast( const std::function< U (const T&) >& func ) const
	{
		if(m == Constant)
			return AST_<U>(func(value));

		std::vector< AST_<U> > newLegs;

		for (auto& x : legs)
			newLegs.push_back(x.recast(func));
		
		AST_<U> ret(m, newLegs);
		ret.name = name;
		
		if (castAllValues)
			ret.value = func(value);

		return ret;
	}

	bool operator==(const AST_& other) const
	{
		if (legs.size() != other.legs.size())
			return false;

		if (m == Binding)
		{
			std::map<std::string, const AST_*>  defs1;
			for (size_t i = 1; i < legs.size(); i++)
				defs1.insert({ legs[i].name, &legs[i] });
			

			for (size_t i = 1; i < other.legs.size(); i++)
			{
				auto find = defs1.find(other.legs[i].name);

				if (find == defs1.end())
					return false;

				if (*(find->second) != other.legs[i])
					return false;
			}

			return legs[0] == other.legs[0];
		}

		if (m == Def && legs.size() == 0)
			return false;


		bool ize = true;// legs == other.legs;//std::equal(legs.cbegin(), legs.cend(), other.legs.cbegin());

		for (size_t i = 0; i < legs.size(); i++)
			ize &= legs[i] == other.legs[i];

		return m == other.m && ize && (m == Constant ? value == other.value : true) && name == other.name;
	}

	bool operator!=(const AST_& other) const
	{
		return !operator==(other);
	}

};



template <class T>
AST_<T> sqrt(const AST_<T>& a)
{
	return AST_<T>(Sqrt, a);
}

template <class T>
AST_<T> pow(const AST_<T>& a, const AST_<T>& b)
{
	return AST_<T>(Pow, a, b);
}

template <class T>
AST_<T> log(const AST_<T>& a)
{
	return AST_<T>(Ln, a);
}

template <class T>
AST_<T> sin(const AST_<T>& a)
{
	return AST_<T>(Sin, a);
}

template <class T>
AST_<T> cos(const AST_<T>& a)
{
	return AST_<T>(Cos, a);
}
