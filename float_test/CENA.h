#pragma once

#include "Szam.h"

template<class V, class E>
class Szam<Method::CENA, V, E> {

	V value;
	E error;
	

public:

	static constexpr V eps = std::numeric_limits<V>::epsilon();
	static constexpr int digits = std::numeric_limits<V>::digits;

	Szam()
	{
		this->value = V(0.0);
		this->error = E(0.0);
	}
	
	template <class V_, typename = std::enable_if_t< std::is_arithmetic< V_ >::value > >
	Szam(V_ value, bool forceError = false)
	{
		this->value = (V) value;
		this->error = (value == floor(value) || !forceError) ? (E) (value - V(value)) : (E) (eps * abs(value) );
	}

	template <class V_, typename = std::enable_if_t< std::is_arithmetic< V_ >::value > >
	Szam(V_ value, E relError)
	{
		this->value = (V) value;
		this->error = (E) ((relError* value) + (value - V(value) ) );
	}

	Szam(AbsoluteError dummy)
	{
		this->value = V(0.0);
		this->error = E(0.0);
	}

	template <class V_, typename = std::enable_if_t< std::is_arithmetic< V_ >::value > >
	Szam(AbsoluteError dummy, V_ value, E error = 0.0)
	{
		this->value = (V) value;
		this->error = (E) (error + ( value - V(value) ) );
	}

	Szam& operator+=(const Szam& other)
	{
		const V newValue = value + other.value;
		error = (E) (-((newValue - (abs(value) > abs(other.value) ? value : other.value)) - (abs(value) > abs(other.value) ? other.value : value)) + error + other.error);
		value = newValue;
		return *this;
	}

	Szam operator+(const Szam& other) const
	{
		return Szam(*this) += other;
	}

	Szam operator-()
	{
		return Szam(AbsoluteError(), -value, -error);
	}

	Szam& operator-=(const Szam& other)
	{
		const V newValue = value - other.value;
		error = (E) (-((newValue - (abs(value) > abs(other.value) ? value : -other.value)) - (abs(value) > abs(other.value) ? -other.value : value)) + error - other.error);
		value = newValue;
		return *this;
	}

	Szam operator-(const Szam& other) const
	{
		return Szam(*this) -= other;
	}

	Szam& operator*=(const Szam& other)
	{
		const V newValue = value * other.value;
		const V M = (V) (std::pow(2, (digits+1) / 2) + 1.0);
		const V yU = (value - value * M) + value * M, yL = value - yU, zU = (other.value - other.value * M) + other.value * M, zL = other.value - zU;
		const E repError = (E) ((-newValue + yU * zU + (yU * zL + yL * zU) + yL * zL));
		error = (E) (other.error * error + repError + other.value * error + value * other.error);
		value = newValue;
		return *this;
	}

	Szam operator*(const Szam& other) const
	{
		return Szam(*this) *= other;
	}

	Szam operator/=(const Szam& other)
	{
		const V newValue = value / other.value;
		const V M = (V) (std::pow(2, (digits+1) / 2) + 1.0);
		const V yU = (newValue - newValue * M) + newValue * M, yL = newValue - yU, zU = (other.value - other.value * M) + other.value * M, zL = other.value - zU;
		const E repError = (E) ( (newValue * other.value - value - (-newValue * other.value + yU * zU + (yU * zL + yL * zU) + yL * zL)) / other.value );
		error = (E) ((error - newValue * other.error) / (other.value + other.error) + repError);
		value = newValue;
		return *this;
	}

	Szam operator/(const Szam& other) const
	{
		return Szam(*this) /= other;
	}

	V getValue() const
	{
		return value;
	}

	E getError() const
	{
		return error;
	}

	E getRelError() const
	{
		return (value == 0.0 && error == 0.0) ? 0 : error / (value);
	}

};

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::CENA, V, E> sqrt(const SZ<Method::CENA, V, E>& x)
{
	const V newValue = (V) (sqrt(x.getValue()));
	const V M = (V) (std::pow(2.0, (SZ<Method::CENA, V, E>::digits+1) / 2) + 1.0);
	const V xU = (newValue - newValue * M) + newValue * M, xL = newValue - xU;
	const E repError = (E) (((- newValue * newValue + x.getValue() - (-newValue * newValue + xU * xU + (xU * xL + xL * xU) + xL * xL))) / (2.0 * newValue));
	const E error = (E) (x.getError() * 0.5 / newValue + repError);
	return SZ<Method::CENA, V, E>(AbsoluteError(), newValue, error);
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::CENA, V, E> abs(const SZ<Method::CENA, V, E>& x)
{
	return SZ<Method::CENA, V, E>(AbsoluteError(), abs(x.getValue()), (x.getValue() < 0) ? -x.getError() : x.getError());
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::CENA, V, E> pow(const SZ<Method::CENA, V, E>& x, const SZ<Method::CENA, V, E>& y)
{
	//(a+alpha)^(b+beta)
	//beta * a^b * ln(a) + alpha * b * a ^ (b-1)
	const V value = (V) (pow(x.getValue(), y.getValue()));
	const E repError = (E) ((pow(dd_real(x.getValue()), dd_real(y.getValue())) - value).toDouble());
	const E error = (E) (repError + value * y.getError() * log(abs(x.getValue())) + x.getError() * y.getValue() * pow(x.getValue(), y.getValue() - 1));
	return SZ<Method::CENA, V, E>(AbsoluteError(), value, error);
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::CENA, V, E> log(const SZ<Method::CENA, V, E>& x)
{
	const V value = (V) (log(x.getValue()));
	const E repError = (E) (( log(dd_real(x.getValue())) - value).toDouble());
	const E error = (E) (x.getError() / x.getValue() + repError);
	return SZ<Method::CENA, V, E>(AbsoluteError(), value, error);
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::CENA, V, E> cos(const SZ<Method::CENA, V, E>& x)
{
	const V value = (V) (cos(x.getValue()));
	const E repError = (E) ((cos(dd_real(x.getValue())) - value).toDouble());
	const E error = (E) (-x.getError() * sin(x.getValue()) + repError);
	return SZ<Method::CENA, V, E>(AbsoluteError(), value, error);
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::CENA, V, E> sin(const SZ<Method::CENA, V, E>& x)
{
	const V value = (V) (sin(x.getValue()));
	const E repError = (E) ((sin(dd_real(x.getValue())) - value).toDouble());
	const E error = (E) (x.getError() * cos(x.getValue()) + repError);
	return SZ<Method::CENA, V, E>(AbsoluteError(), value, error);
}