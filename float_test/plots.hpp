#pragma once

#include "precomp.hpp"
#include <future>

extern AST<dd_real> expression, expr_noSN;
extern const double SunToSec, c, G, mSun;

template <class T>
AST<T> getExpression(bool needsSNMultiplier)
{
	if (needsSNMultiplier)
		return expression.recast<T>([](const dd_real& x) {return x; });
	else
		return expr_noSN.recast<T>([](const dd_real& x) {return x; });
}

//# of threads
const int NoT = 5;

template <typename T>
struct DataPoint {
	double x, y;
	T val;
public:
	DataPoint(double x, double y, T val) : x(x), y(y), val(val) {}
};

//Check error of self detection for a bunch of relativ errors
//Indexed for parallel execution
template <typename T, bool needsSNMultiplier = true>
std::vector< DataPoint<decltype(errorOf(T()))> > MassErrorThread(std::map< std::string, T> con, double m1, double m2, int N, int M, std::pair<double, double> xRange, std::pair<double, double> yRange, int ind)
{
	using DP = DataPoint<decltype(errorOf(T()))>;

	std::vector< DP > ret;

	auto expr = getExpression<T>( needsSNMultiplier );

	for (int i = (ind * N) / NoT; i < (ind * N + N) / NoT; i++)
	{
		for (int j = 0; j < M; j++)
		{
			double m1Err = xRange.first + ((double)i / (N - 1)) * (xRange.second - xRange.first);
			double m2Err = yRange.first + ((double)j / (M - 1)) * (yRange.second - yRange.first);

			con["m1_1"] = T(m1, m1Err);
			con["m2_1"] = T(m2, m2Err);

			ret.push_back(DP(m1Err, m2Err, errorOf( expr.boundC(con).simplify().evaluate( ) )));
		}
	}

	return ret;
}

//Check error of self detection for a range of relative errors
template <typename T, bool needsSNMultiplier = true>
void doGWMassError(std::pair<double, double> xRange, std::pair<double, double> yRange, int N, int M, double m1Sun = 41, double m2Sun = 20, const std::string& fname = "massErrors")
{
	using DP = DataPoint<decltype(errorOf(T()))>;

	double m1 = m1Sun * SunToSec, m2 = m2Sun * SunToSec;
	
	//Context
	std::map<std::string, T> con({ {"m1_1", m1 },{ "m2_1", m2 },{ "m1_2", m1 },{ "m2_2", m2 },{ "tC", 0.0 }, { "phiC", 0.0 } });

	//output file
	std::ofstream out(fname + "_m1_" + std::to_string(m1Sun) + "_m2_" + std::to_string(m2Sun) + "_" + getSzamName(T()) + ".dat");

	//threads
	std::vector< std::future< std::vector< DP > > > futs(NoT);
	//Create threads
	for (int t = 0; t < NoT; t++)
		futs[t] = std::async(std::launch::async, MassErrorThread<T, needsSNMultiplier>, con, m1, m2, N, M, xRange, yRange, t);

	//Wait threads
	for (auto&& fut : futs)
	{
		auto ret = fut.get();
		for (const DP& p : ret)
			out << p.x << "\t" << p.y << "\t" << p.val << std::endl;
	}

	out.close();
}

//Check error of self detection for a bunch of different masses
//Indexed for parallel execution
template <typename T, bool needsSNMultiplier = true>
std::vector< DataPoint<decltype(errorOf(T()))> > MassFloatingRelErrorThread(std::map< std::string, T > con, double m1, double m2, int N, int M, std::pair<double, double> xRange, std::pair<double, double> yRange, int ind)
{
	using DP = DataPoint<decltype(errorOf(T()))>;

	std::vector< DP > ret;

	auto expr = getExpression<T>( needsSNMultiplier );

	for (int i = (ind * N) / NoT; i < (ind * N + N) / NoT; i++)
	{
		for (int j = 0; j < M; j++)
		{
			double m1Err = xRange.first + ((double)i / (N - 1)) * (xRange.second - xRange.first);
			double m2Err = yRange.first + ((double)j / (M - 1)) * (yRange.second - yRange.first);

			con["m1_1"] = T(m1 * (1 + m1Err));
			con["m2_1"] = T(m2 * (1 + m2Err));

			ret.push_back(DP(m1Err, m2Err, errorOf( expr.boundC(con).simplify().evaluate() )));
		}
	}

	return ret;
}

//Check error of self detection for a bunch of different masses
//Indexed for parallel execution
template <typename T, bool needsSNMultiplier = true>
std::vector< DataPoint<decltype(errorOf(T()))> > MassFloatingErrorThread(std::map< std::string, T > con, int N, int M, std::pair<double, double> xRange, std::pair<double, double> yRange, int ind)
{
	using DP = DataPoint<decltype(errorOf(T()))>;

	std::vector< DP > ret;

	auto expr = getExpression<T>(needsSNMultiplier);

	for (int i = (ind * N) / NoT; i < (ind * N + N) / NoT; i++)
	{
		for (int j = 0; j < M; j++)
		{
			double m1 = xRange.first + ((double)i / (N - 1)) * (xRange.second - xRange.first);
			double m2 = yRange.first + ((double)j / (M - 1)) * (yRange.second - yRange.first);

			con["m1_1"] = T(m1);
			con["m2_1"] = T(m2);
			con["m1_2"] = T(m1);
			con["m2_2"] = T(m2);

			ret.push_back(DP(m1 / SunToSec, m2 / SunToSec, errorOf( expr.boundC(con).simplify().evaluate() )));
		}
	}

	return ret;
}

//Check error of self detection for a range of relative errors
template <typename T, bool needsSNMultiplier = true>
void doGWMassFloatingError(std::pair<double, double> xRange, std::pair<double, double> yRange, int N, int M, const std::string& fname)
{
	using DP = DataPoint<decltype(errorOf(T()))>;

	std::map<std::string, T> con({ { "tC", 0.0 },{ "phiC", 0.0 } });
	
	//output file
	std::ofstream out(fname + getSzamName(T()) + ".dat");

	//threads
	std::vector< std::future< std::vector< DP > > > futs(NoT);
	//Create threads
	for (int t = 0; t < NoT; t++)
		futs[t] = std::async(std::launch::async, MassFloatingErrorThread<T, needsSNMultiplier>, con, N, M, xRange, yRange, t);

	//Wait threads
	for (auto&& fut : futs)
	{
		auto ret = fut.get();
		for (DP& p : ret)
			out << p.x << "\t" << p.y << "\t" << p.val << std::endl;
	}

	out.close();

}

//Check an m1* m2* mass template against a con2 template
//Indexed for parallel execution
template <typename T, bool needsSNMultiplier = true>
std::vector< DataPoint<T> > DetectionErrorThread(std::map< std::string, T> con, int N, int M, std::pair<double, double> xRange, std::pair<double, double> yRange, int ind)
{
	using DP = DataPoint<T>;

	std::vector< DP > ret;

	auto expr = getExpression<T>(needsSNMultiplier);

	for (int i = (ind * N) / NoT; i < (ind * N + N) / NoT; i++)
	{
		for (int j = 0; j < M; j++)
		{
			double m1 = (xRange.first + ((double)i / (N - 1)) * (xRange.second - xRange.first)) * SunToSec;
			double m2 = (yRange.first + ((double)j / (M - 1)) * (yRange.second - yRange.first)) * SunToSec;

			con["m1_1"] = T(m1);
			con["m2_1"] = T(m2);

			ret.push_back(DP(m1 / SunToSec, m2 / SunToSec, expr.boundC(con).simplify().evaluate()));
		}
	}

	return ret;
}

//Check an m1* m2* mass template against an m1 m2 template
template <typename T, bool needsSNMultiplier = true>
void doGWDetectionError(std::pair<double, double> xRange, std::pair<double, double> yRange, double m1, double m2, int N, int M, const std::string& fname)
{
	using DP = DataPoint<T>;

	std::map<std::string, T> con({ { "m1_2", m1 * SunToSec }, {"m2_2", m2 * SunToSec }, { "tC", 0.0 },{ "phiC", 0.0 } });

	std::ofstream out(fname + "_" + std::to_string(m1) + "_" + std::to_string(m2) + "_" + getSzamName(T()) + ".dat");

	std::vector< std::future< std::vector< DP > > > futs(NoT);

	//Create threads
	for (int t = 0; t < NoT; t++)
		futs[t] = std::async(std::launch::async, DetectionErrorThread<T, needsSNMultiplier>, con, N, M, xRange, yRange, t);

	//Wait threads
	for (auto&& fut : futs)
	{
		auto ret = fut.get();
		for (DP& p : ret)
			out << p.x << "\t" << p.y << "\t" << valueOf(p.val) << "\t" << errorOf(p.val) << std::endl;
	}

	out.close();

}
