#pragma once
#include <initializer_list>
#include <math.h>
#include <numeric>
#include <vector>
#include <utility>
#include <iterator>
#include "Vector.h"

struct Dummy {};

//namespace MyMath{

template<class T>
class Matrix;

template<class T>
Matrix<T>& operator*=(Matrix<T>& m, const Matrix<T>& rhs);

template<class T>
class Matrix
{
	std::vector< Vector<T> > tomb;

	size_t N, M;
public:

	Matrix(size_t N = 3, size_t M = 3)
	{
		this->N = N;
		this->M = M;

		tomb = std::vector< Vector<T> >(N, Vector<T>(M));
	}

	Matrix(const Matrix<T>& masik)
	{
		N = masik.N;
		M = masik.M;

		tomb = masik.tomb;
	}

	Matrix(Matrix<T>&& masik)
	{
		N = masik.N;
		M = masik.M;

		tomb = std::move(masik.tomb);
	}

	template<class Iter>
	Matrix(const Iter& begin, const Iter& end, Dummy dummy)
	{
		N = std::distance(begin, end);
		tomb = std::vector< Vector<T> >(begin, end);
	}

	Matrix(const std::initializer_list< Vector<T> >& list)
	{
		N = list.size();
		tomb = std::vector< Vector<T> >(list);
		M = tomb[0].getDimension();
	}
	
	~Matrix()
	{
	}

	Vector<T>& operator[](size_t ind)
	{
		return tomb[ind];
	}

	const Vector<T>& operator[](size_t ind) const
	{
		return tomb[ind];
	}

	size_t getDimensionN() const
	{
		return N;
	}

	size_t getDimensionM() const
	{
		return M;
	}

	std::pair<size_t, size_t> getDimensions() const
	{
		return std::pair<size_t, size_t>(N, M);
	}

	T determinant() const
	{
		assert(N == M);
		assert(N == 2);//TODO csak 2x2-eset sz�molok
		return tomb[0][0] * tomb[1][1] - tomb[0][1] * tomb[1][0];
	}

	///Row iterator
	typename std::vector< Vector<T> >::iterator begin()
	{
		return tomb.begin();
	}

	///Row iterator
	typename std::vector< Vector<T> >::const_iterator begin() const
	{
		return tomb.begin();
	}

	//Row iterator
	typename std::vector< Vector<T> >::const_iterator cbegin() const
	{
		return tomb.cbegin();
	}

	///Row iterator
	typename std::vector< Vector<T> >::iterator end()
	{
		return tomb.end();
	}
	
	//Row iterator
	typename std::vector< Vector<T> >::const_iterator end() const
	{
		return tomb.end();
	}

	///Row iterator
	typename std::vector< Vector<T> >::const_iterator cend() const
	{
		return tomb.cend();
	}

	Matrix<T>& operator+=(const Matrix<T>& rhs)
	{
		assert(N == rhs.N && M = rhs.M);
		for (unsigned int i = 0; i < N; i++)
			tomb[i] += rhs.tomb[i];
		return *this;
	}

	Matrix<T>& operator-=(const Vector<T>& rhs)
	{
		assert(N == rhs.N && M = rhs.M);
		for (unsigned int i = 0; i < N; i++)
			tomb[i] -= rhs.tomb[i];
		return *this;
	}

	template<class U>
	Matrix<T>& operator*=(const U& rhs)
	{
		for (auto& x : *this)
			x *= rhs;
		return *this;
	}

	/*
	template<>
	Matrix& operator*=(const Matrix& rhs) //TODO Microsoft specialization o_O
	{
		assert(M == rhs.N);

		std::vector< Vector<T> > newTomb(N, Vector<T>(rhs.M));

		for (unsigned int i = 0; i < N; i++)
			for (unsigned int k = 0; k < rhs.M; k++)
				for (unsigned int j = 0; j < M; j++)
					newTomb[i][k] += tomb[i][j] * rhs.tomb[j][k];

		tomb = std::move(newTomb);
		M = rhs.M;

		return *this;
	}
*/

	template<class U>
	Matrix<T>& operator/=(const U& rhs)
	{
		for (auto& x : *this)
			x /= rhs;
		return *this;
	}

	Matrix<T>& operator=(const Matrix<T>& rhs)
	{
		N = rhs.N;
		M = rhs.M;
		
		tomb = rhs.tomb;
		return *this;
	}

	Matrix<T>& operator=(Matrix<T>&& rhs)
	{
		N = rhs.N;
		M = rhs.M;

		tomb = std::move(rhs.tomb);
		return *this;
	}

	class columnIterator : public std::iterator<std::forward_iterator_tag, T >
	{
		size_t c, r;
		Matrix& m;
	public:
		columnIterator(Matrix& mat, size_t column, size_t row) : m(mat), c(column), r(row) { }

		columnIterator& operator++() { r++; return *this; }
		columnIterator operator++(int) { columnIterator retval = *this; ++(*this); return retval; }
		bool operator==(columnIterator other) const { return c == other.c && r == other.r; }
		bool operator!=(columnIterator other) const { return !(*this == other); }

		T& operator*() const { return m.tomb[r][c]; }
	};

	columnIterator beginColumn(size_t c)
	{
		assert(c < M);
		return columnIterator(*this, c, 0);
	}

	columnIterator endColumn(size_t c)
	{
		assert(c < M);
		return columnIterator(*this, c, N);
	}

	template <class U>
	friend Matrix<U>& operator*=(Matrix<U>& m, const Matrix<U>& rhs);

};

template<class T>
std::ostream& operator<<(std::ostream& os, const Matrix<T>& obj)
{
	for (const Vector<T>& x : obj)
		os << x << "\n";
	return os;
}

template<class T>
std::istream& operator>>(std::istream& is, Matrix<T>& obj)
{
	for (Vector<T>& x : obj)
		if (!(is >> x))
			break;
	return is;
}

template<class T>
inline Matrix<T> operator+(Matrix<T>  lhs, const Matrix<T>& rhs)
{
	lhs += rhs;
	return lhs;
}

template<class T>
inline Matrix<T> operator+(const Matrix<T>&  lhs, Matrix<T>&& rhs)
{
	Matrix<T> ret(rhs);
	ret += lhs;
	return ret;
}


template<class T>
inline Matrix<T> operator-(Matrix<T> lhs, const Matrix<T>& rhs)
{
	lhs -= rhs;
	return lhs;
}

template<class T>
inline Matrix<T> operator-(const Matrix<T>&  lhs, Matrix<T>&& rhs)
{
	Matrix<T> ret(rhs);
	for (unsigned int i = 0; i < ret.getDimensionN(); i++)
		ret[i] = lhs[i] - ret[i];
	return ret;
}

template<class T, class U>
inline Matrix<T> operator*(Matrix<T> lhs, const U& rhs)
{
	lhs *= rhs;
	return lhs;
}

template<class T, class U>
inline Matrix<T> operator*(const U& rhs, Matrix<T> lhs)
{
	lhs *= rhs;
	return lhs;
}

template<class T>
inline Matrix<T> operator*(Matrix<T> lhs, const Matrix<T>& rhs)
{
	lhs *= rhs;
	return lhs;
}

template<class T, class U>
inline Matrix<T> operator/(Matrix<T> lhs, const U& rhs)
{
	lhs /= rhs;
	return lhs;
}

template<class T>
Vector<T> operator*(const Matrix<T>& M, const Vector<T>& V)
{
	assert(M.getDimensionM() == V.getDimension());
	Vector<T> ret(M.getDimensionN());//TODO k�ne valami el�re nem inicializ�l� megold�s, hogy ne legyen 2-szer m�solva minden vektor k�sz�t�sn�l, hanem ut�lag felt�lteni ink�bb..
	
	for (int i = 0; i < ret.getDimension(); i++)
		ret[i] = skalar(M[i], V);

	return ret;
}

template<class T>
Matrix<T>& operator*=(Matrix<T>& m, const Matrix<T>& rhs) //TODO Microsoft specialization o_O
{
	assert(m.M == rhs.N);

	std::vector< Vector<T> > newTomb(m.N, Vector<T>(rhs.M));

	for (unsigned int i = 0; i < m.N; i++)
		for (unsigned int k = 0; k < rhs.M; k++)
			for (unsigned int j = 0; j < m.M; j++)
				newTomb[i][k] += m.tomb[i][j] * rhs.tomb[j][k];

	m.tomb = std::move(newTomb);
	m.M = rhs.M;

	return m;
}


//}

// Local Variables:
// mode: c++
// End:
