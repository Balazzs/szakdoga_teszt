#pragma once

#include <iostream>
#include <algorithm>
#include <iomanip>
#include <random>

#ifndef M_PI
constexpr auto M_PI = 3.14159265358979323846;
#endif

#include "Matrix.hpp"
#include "Szam.h"
#include "integrals.hpp"


template<class T,  class F>
T integrate(const std::vector< std::pair<T, T> >& xw, const F& f)
{
	return std::accumulate(xw.begin(), xw.end(), T(0.0), [&f](auto a, auto b) {return a + f(b.first) * b.second;});
}

template <class T>
Matrix<T> gaussianElimFullPivoting(Matrix<T> m)
{
	const size_t N = std::min(m.getDimensionN(), m.getDimensionM() - 1);

	std::vector<size_t> columns(N);

	for (size_t n = 0; n < N; n++)
	{
		size_t x, y;

		T max(0);

		for (size_t l = n; l < m.getDimensionN(); l++)
			for (size_t j = 0; j < m.getDimensionM() - 1; j++)
			{
				if (abs(m[l][j]) >= abs(max))
				{
					max = m[l][j];
					x = j;
					y = l;
				}
			}

		std::iter_swap(m.begin() + n, m.begin() + y);
		columns[n] = x;

		m[n] /= max;

		for (size_t n2 = n + 1; n2 < m.getDimensionN(); n2++)
			m[n2] -= m[n] * m[n2][x];
	}
			
	for (size_t n = N ; n-- ;)//unsigned backwards loop | smiley ";)" included
	{
		const T val = m[n][ columns[n] ];

		m[n] /= val;

		for (size_t n2 = n; n2-- ;)
			m[n2] -= m[n] * m[n2][ columns[n] ];
	}

	for (size_t n = 0; n < N; n++)
		for (size_t j = 0; j < N; j++)
		{
			if (columns[j] == n)
			{
				iter_swap(m.begin() + n, m.begin() + j);
				columns[j] = columns[n];
				columns[n] = n;
				break;
			}
		}

	return m;
}

template <class T>
Matrix<T> gaussianElim(Matrix<T> m)
{
	const size_t N = std::min(m.getDimensionN(), m.getDimensionM());

	for (size_t n = 0; n < N; n++)
	{
		const T first = m[n][n];
		
		m[n] /= first;

		for (size_t n2 = n + 1; n2 < m.getDimensionN(); n2++)
			m[n2] -= m[n] * m[n2][n];
	}

	for (size_t n = N; n-- ;) //unsigned backwards loop
	{
		const T val = m[n][n];

		m[n] /= val;

		for (size_t n2 = n ; n2-- ;)
			m[n2] -= m[n] * m[n2][n];
	}

	return m;
}

template <class T, bool defaultSeed = false>
void doTests()
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_real_distribution<double> dist(0.5, 1.5);//TODO ez egyel�re mindig double

	if(defaultSeed)
		mt.seed();

	std::cout << std::endl << std::string(40, '-').c_str() << std::endl;
	std::cout << "Started testing " << typeid(T).name() << std::endl << std::endl;

	T a(500), b(501);

	std::cout << "sqrt(b*b-a*a):" << std::endl;
	
	for (int i = 2; i < 6; i++)
	{
		const int aa = 5 * (int) pow(10, i), bb = 5 * (int) pow(10, i) + 1;
		
		a = T(aa);
		b = T(bb);

		std::cout << "a = " << aa << "; b = " << bb << std::endl;
		std::cout << sqrt(b*b - a*a) << std::endl;
	}

	std::cout << std::endl;
	
	const unsigned int vecSize = 10;

	Vector<T> v1(vecSize), v2(vecSize);

	for (T& x : v1)
		x = T(dist(mt));

	for (T& x : v2)
		x = T(dist(mt));

	std::cout << "Vektor 1:" << std::endl << v1 << std::endl;
	std::cout << "Vektor 2:" << std::endl << v2 << std::endl;

	std::cout << "v1 + v2:" << std::endl << v1 + v2 << std::endl;

	std::cout << "v1 * v2:" << std::endl << skalar(v1, v2) << std::endl;

	Matrix<T> m(vecSize, vecSize), m2(vecSize, vecSize);

	for (int i = 0; i < vecSize; i++)
			m[i][i] = 1.0;

	std::cout << "I" << std::endl << m << std::endl;

	std::cout << "I * Vektor 1:" << std::endl << m * v1 << std::endl;

	for (int i = 0; i < vecSize; i++)
		for (int j = 0; j < vecSize; j++)
		{
			m[i][j] = dist(mt);
			m2[i][j] = dist(mt);
		}


	std::cout << "M1" << std::endl << m << std::endl;
	std::cout << "M2" << std::endl << m2 << std::endl;

	std::cout << "M * Vektor 1:" << std::endl << m * v1 << std::endl << std::endl;

	std::cout << "M1 * M2" << std::endl << m * m2 << std::endl;



	const int Nx = 1000;
	std::vector< std::pair<T, T> > xw(Nx);
	for (int i = 0; i < Nx; i++)
		xw[i] = std::pair<T, T>(i * 2 * M_PI / Nx, 2 * M_PI / Nx);

	std::cout << std::endl << "Integral " << Nx << " ponton 0-2PI intervallumon:" << std::endl;
	std::cout << integrate(xw, [](T x) { return x*x; }) << std::endl;

	std::vector< std::pair<T, T> > chebxw(Nx);
	for (int i = 0; i < Nx; i++)
	{
		const T xi = T(cos((2 * i - 1) / (2.0 * Nx) * M_PI)); //NOTE nem veszi figyelembe a bels� hib�kat
		chebxw[i] = std::pair<T, T>( xi, T(M_PI / Nx) * sqrt( T(1) - xi * xi) );
	}

	std::cout << "-1 - 1 intervallumon Chebyshev bazison:" << std::endl;
	std::cout << integrate(chebxw, [](T x) { return x*x; }) << std::endl;


	const int x = 4, y = 4;
	Matrix<T> mat(y, x+1);

	for (int i = 0; i < y; i++)
		for (int j = 0; j < x+1; j++)
			mat[i][j] = dist(mt);

	std::cout << std::setprecision(2);

	std::cout << std::endl << "Gauss eliminacio random matrixon:" << std::endl << mat << std::endl;

	Matrix<T> elim = gaussianElim(mat);

	std::cout << std::endl << "A matrix vegso alakja:" << std::endl << elim << std::endl;

	std::cout << std::endl << "Az eredmenyvektor:" << std::endl << Vector<T>(elim.beginColumn(x), elim.endColumn(x)) << std::endl;

	Matrix<T> elim2 = gaussianElimFullPivoting(mat);

	std::cout << std::endl << "Teljes pivotinggal:" << std::endl << elim2 << std::endl;

	std::cout << std::endl << "Az eredmenyvektor:" << std::endl << Vector<T>(elim2.beginColumn(x), elim2.endColumn(x)) << std::endl;

	std::cout << std::endl << "Rosszul kondicionalt matrix:" << std::endl << "[[1, 1] [1, 1+eps]] * [a, b] = [1, 1+eps]" << std::endl;

	std::cout << std::setprecision(20);

	T epsPlus1 = 1+1e-10;
	Matrix<T> illCond2 = { { 1, 1, 1}, { 1, epsPlus1, epsPlus1 } };
	Matrix<T> illCond = { { 1, 1 },{ 1, epsPlus1} };
	Matrix<T> inverse = Matrix<T>({ {illCond[1][1], illCond[0][0]}, {-illCond[1][0], -illCond[0][1] } }) / illCond.determinant();

	elim = gaussianElim(illCond2);
	elim2 = gaussianElimFullPivoting(illCond2);

	std::cout << std::endl << illCond2 << std::endl;

	std::cout << std::endl << "Determinans: " << illCond.determinant() << std::endl;

	std::cout << std::endl << "Inverz: " << inverse << std::endl;

	std::cout << std::endl << "Megoldas gauss (0 pivot, full pivot):" << std::endl << Vector<T>(elim.beginColumn(2), elim.endColumn(2)) << std::endl << Vector<T>(elim2.beginColumn(2), elim2.endColumn(2)) <<std::endl;


	/*

	Matrix<T> nagyMat = {
		{1.0000000000000000, 8.0000000000000000, 6.0000000000000000, 12.0000000000000000, 24.0000000000000000, 24.0000000000000000, 8.0000000000000000, 6.0000000000000000, 24.0000000000000000, 24.0000000000000000, 24.0000000000000000,														1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0},
		{4.5999999999999996, 41.8531411531233601, 33.0479488942856037, 87.8349057232554173, 149.3783917109033439, 195.3689938163366833, 121.0451669808013690, 48.8422484540841708, 223.6406089026404516, 851.8470736603384239, 269.3015780207464900,											0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0},
		{21.1599999999999966, 218.9606780479085160, 182.0278210198854936, 642.9142219510971472, 929.7459962556697519, 1590.3768227003254196, 1831.4915561762611560, 397.5942056750813549, 2083.9634145976574473, 30235.1432043200838962, 3021.8058301860087340,									0,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0},
		{97.3359999999999701, 1145.5240206653393216, 1002.6076877338904296, 4705.8591727678940515, 5786.8317341801457587, 12946.2633183243797248, 27711.6501551604087581, 3236.5658295810949312, 19419.1186238102454809, 1073154.9275125553831458, 33907.3782725576675148,						0,	0,	0,	1,	0,	0,	0,	0,	0,	0,	0},
		{447.7455999999998539, 5992.9723163999815370, 5522.3546042079124163, 34444.8913989153879811, 36017.8173980603314703, 105387.4349242659372976, 419295.1650431178859435, 26346.8587310664843244, 180954.3130575636751018, 38090161.8577392920851707, 380471.2698060897528194,				0,	0,	0,	0,	1,	0,	0,	0,	0,	0,	0},
		{0.0000000000000000, 34.2801357124991952, 168.4702728821191613, 2101.6181209908259007, 1236.1435394200643714, 6631.0420254749351443, 38374.2674650820554234, 4069.0485156323466072, 28291.8793721561523853, 7044717.1197200166061521, 60211.4334496619121637,							0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	0},
		{2059.6297599999993508, 31353.0895356311411888, 30417.0821226643129194, 252121.9823892920394428, 224178.4848274685500655, 857893.2134182706940919, 6344206.6583608603104949, 214473.3033545676735230, 1686197.1981563565786928, 1351958038.0734937191009521, 4269229.7229307144880295,	0,	0,	0,	0,	0,	0,	1,	0,	0,	0,	0},
		{0.0000000000000000, 179.3414198404317403, 927.9328280691040618, 15382.9524602928686363, 7693.8805767663707229, 53979.1670196200575447, 580627.4516345988959074, 33123.5797620395824197, 263633.8804078772664070, 250042569.2999326586723328, 675626.4184535464737564,					0,	0,	0,	0,	0,	0,	0,	1,	0,	0,	0},
		{0.0000000000000000, 938.2502198978935439, 5111.0461132262771571, 112596.6815912620077142, 47887.4794405465727323, 439410.6478194649680518, 8785268.3545934017747641, 269638.3520710353623144, 2456635.0642409822903574, 8874917956.1941699981689453, 7581135.8600852200761437,			0,	0,	0,	0,	0,	0,	0,	0,	1,	0,	0},
		{0.0000000000000000, 938.2502198978935439, 0.0000000000000000, 56298.3407956310038571, 23943.7397202732863661, 319571.3802323381532915, 8785268.3545934017747641, 0.0000000000000000, 269630.6777825467870571, 3293783983.7421655654907227, 1735440.7390556528698653,					0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	0},
		{0.0000000000000000, 70.9608494071368625, 1546.2151390406352220, 34063.2210755480555235, 13279.8613116998949408, 129911.1650312914862297, 2657756.2850107550621033, 183537.2854802548536099, 1654054.3836708476301283, 5487391301.6329326629638672, 5049794.3807012736797333,			0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1}
		};
	
	auto nagyElim = gaussianElim(nagyMat);
	auto nagyElim2 = gaussianElimFullPivoting(nagyMat);

	std::cout << nagyElim << std::endl;

	std::cout << std::setprecision(3);

	for(int j = 0; j < 11; j++)
	{
		for(int i = 11; i < 22; i++)
			std::cout << relErrorOf(nagyElim[j][i]) << "\t";
		std::cout << std::endl;
	}

	std::cout << std::endl;
	std::cout << std::endl;

	for(int j = 0; j < 11; j++)
	{
		for(int i = 11; i < 22; i++)
			std::cout << relErrorOf(nagyElim2[j][i]) << "\t";
		std::cout << std::endl;
	}

	std::cout << std::endl;*/
}

template <class T>
void illConditioned()
{
	T a(500), b(501);

	std::cout << "sqrt(b*b-a*a):" << std::endl;

	for (int i = 2; i < 6; i++)
	{
		const int aa = 5 * (int)pow(10, i), bb = 5 * (int)pow(10, i) + 1;

		a = T(aa);
		b = T(bb);

		std::cout << "a = " << aa << "; b = " << bb << std::endl;
		std::cout << sqrt(b*b - a*a) << std::endl;
	}

	std::cout << std::endl;

	
	std::cout << "1 / (1 - a):" << std::endl;

	for (int i = 1; i < 10; i++)
	{
		std::string valS = "0.";
		for (int j = 0; j < i; j++)
			valS += "9";

		a = T(dd_real(valS));

		std::cout << "a = " << valS << std::endl;
		std::cout << T(1.0) / (T(1.0) - a) << std::endl;
	}

	std::cout << std::endl;


	std::cout << "(1 - cos^2(x)) / x^2:" << std::endl;

	for (int i = 1; i < 10; i++)
	{
		std::string valS = "0.";
		for (int j = 0; j < i; j++)
			valS += "0";

		valS += "1";

		a = T(dd_real(valS));

		std::cout << "x = " << valS << std::endl;
		std::cout << (T(1.0) - cos(a) * cos(a) ) / (a * a) << std::endl;
	}

	std::cout << std::endl;
}

//Get S_N * 10^49 at f freq
template < class T >
T getAtPoint(T f)
{
	T x = f / T(300);
	return  T(1) / (T(dd_real("1.6")) * (T(300) * pow(f, T(-17)) + T(7) * pow(f / T(50), T(-6)) + T(24) * pow(T(300) * x / T(90), T(dd_real("-3.45"))) - T(3.5) / pow(x, T(2)) + T(110) * (T(dd_real("1.02")) - T(dd_real("1.08")) * x * x + T(dd_real("0.54")) * pow(x, T(4))) / (T(1) + T(dd_real("0.21")) * x * x)));
}

//Integral 1/S_n from 10 to 300 Hz with float double and dd_real
void integral(const std::string& fname)
{
	std::ofstream fs(fname);

	fs << std::setprecision(32);

	float sumf = 0.0;
	double sumd = 0.0;
	dd_real sumdd = 0.0;

	for (dd_real f = 10; f < 300;)
	{
		for (int i = 0; i < 1000; i++)
		{
			f += 1e-4;
			sumf += getAtPoint<float>((float)f);
			sumd += getAtPoint<double>((double)f);
			sumdd += getAtPoint<dd_real>(f);
		}
		fs << f << "\t" << sumf << "\t" << sumd << "\t" << sumdd << std::endl;
	}

	fs.close();
}