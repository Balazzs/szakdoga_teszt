import numpy
import math
from collections import defaultdict

arr = numpy.loadtxt("1.dat")

width=1

dicts = [defaultdict(int), defaultdict(int) ,defaultdict(int) ,defaultdict(int), defaultdict(int), defaultdict(int), defaultdict(int), defaultdict(int)]

for y in range(arr.shape[0]):
	for x in range(arr.shape[1]):
		if arr[y][x] == 0.0 or not arr[y][x] == arr[y][x]:
			pos = -999
		else:
			pos = math.log10(abs(arr[y][x]))
		dicts[x][int(math.floor(pos))]+=1

for i in range(-32, 10):
#	for x in range(arr.shape[1]):-
	print(str(i) + "\t" + "\t".join([ str(dicts[x][i]) for x in range(arr.shape[1]) ]))
