#pragma once

#include <map>
#include <string>
#include <cmath>

template <class T>
class AST_;

//A set of variable-value ptr pairs for linking variables to values
template <class T>
struct Scope
{
	std::map<std::string, T*> vars;

public:

	Scope() {}

	Scope(const std::map<std::string, T*>& map)
	{
		vars = map;
	}

	//gets an AST representing the variable with the given name
	//If the name exists in this scope then returns a Constant with its value
	//Otherwise returns a Variable with the given name
	AST_<T> getVar(const std::string& name) const
	{
		auto find = vars.find(name);
		if (find == vars.end())
			return AST_<T>(name);
		else
			return AST_<T>(*(*find).second);
	}

	//Checks whether the scope has a pointer to the given variable
	bool contains(const std::string& name) const
	{
		return vars.end() != vars.find(name);
	}

	//gets the pointer by the given name
	T* get(const std::string& name) const
	{
		auto find = vars.find(name);
		if (find == vars.end())
			return nullptr;
		else
			return (*find).second;
	}

	//gets the value by the given name
	T getValue(const std::string& name) const
	{
		auto find = vars.find(name);
		if (find == vars.end())
			return NAN;
		else
			return *(*find).second;
	}

	//adds a pointer with the given name
	void add(const std::string& name, T* ptr)
	{
		//vars.insert({ name, ptr });
		vars[name] = ptr;
	}

	//Removes the ptr with the given name
	void remove(const std::string& name)
	{
		vars.erase(name);
	}
	
};
