#pragma once

template<class T, class L, class F>
T dumb_integrate(std::pair<L, L> interval, L step, const F& f)
{
	T ret(0.0);
	while (interval.first < interval.second)
	{
		ret += step * f(interval.first);
		interval.first += step;
	}
	return ret;
}

template<class T, class L, class F>
T boole_integrate(std::pair<L, L> interval, L step, const F& f)
{
	T ret(0.0);
	auto prevEnd = f(interval.first);
	while (interval.first < interval.second)
	{
		auto nextEnd = f(interval.first + step);
		ret += step / T(90.0) * (T(7) * prevEnd + T(32) * f(interval.first + step / T(4)) + T(12) * f(interval.first + step / T(2)) + T(32) * f(interval.first + step * T(3)  / T(4)) + T(7) * nextEnd);
		prevEnd = nextEnd;
		interval.first += step;
	}
	return ret;
}

template<class T, class L, class F, class G>
T boole_integrate_do_partial_sums(std::pair<L, L> interval, L step, const F& f, const G& g)
{
	T ret(0.0);
	auto prevEnd = f(interval.first, true);
	while (interval.first < interval.second)
	{
		auto nextEnd = f(interval.first + step, true);
		ret += step / T(90.0) * (T(7) * prevEnd + T(32) * f(interval.first + step / T(4)) + T(12) * f(interval.first + step / T(2)) + T(32) * f(interval.first + step * T(3) / T(4)) + T(7) * nextEnd);
		prevEnd = nextEnd;
		interval.first += step;
		g(ret);
	}
	return ret;
}
