#pragma once
#pragma once

#include "Szam.h"

template<class V, class E>
class Szam<Method::RepEst, V, E> {

	V value;
	E error;

public:

	static constexpr V eps = std::numeric_limits<V>::epsilon();
	static constexpr int digits = std::numeric_limits<V>::digits;

	Szam()
	{
		this->value = V(0.0);
		this->error = E(0.0);
	}

	template <class V_, typename = std::enable_if_t< std::is_arithmetic< V_ >::value > >
	Szam(V_ value, bool forceError = false)
	{
		this->value = (V) value;
		this->error = (value == floor(value) || !forceError) ? (E) abs(value - V(value)) : (E) (eps * abs(value) );
	}

	template <class V_, typename = std::enable_if_t< std::is_arithmetic< V_ >::value > >
	Szam(V_ value, E relError)
	{
		this->value = (V) value;
		this->error = (E) (abs(relError*value) + abs(value - V(value)));
	}

	Szam(AbsoluteError dummy)
	{
		this->value = V(0.0);
		this->error = E(0.0);
	}

	template <class V_, typename = std::enable_if_t< std::is_arithmetic< V_ >::value > >
	Szam(AbsoluteError dummy, V_ value, E error = 0)
	{
		this->value = (V) value;
		this->error = (E) (error + abs(value - V(value)) );
	}

	Szam& operator+=(const Szam& other)
	{
		const V newValue = value + other.value;
		error = (E) (abs((newValue - (abs(value) > abs(other.value) ? value : other.value)) - (abs(value) > abs(other.value) ? other.value : value)) + error + other.error);
		value = newValue;
		return *this;
	}

	Szam operator+(const Szam& other) const
	{
		return Szam(*this) += other;
	}

	Szam operator-()
	{
		return Szam(AbsoluteError(), -value, error);
	}

	Szam& operator-=(const Szam& other)
	{
		const V newValue = value - other.value;
		error = (E) (abs((newValue - (abs(value) > abs(other.value) ? value : -other.value)) - (abs(value) > abs(other.value) ? -other.value : value)) + error + other.error);
		value = newValue;
		return *this;
	}

	Szam operator-(const Szam& other) const
	{
		return Szam(*this) -= other;
	}

	Szam& operator*=(const Szam& other)
	{
		const V newValue = value * other.value;
		const V M = (V) (pow(2, (digits+1) / 2) + 1.0);
		const V yU = (value - value * M) + value * M, yL = value - yU, zU = (other.value - other.value * M) + other.value * M, zL = other.value - zU;
		const E repError = (E) abs( -newValue + yU * zU + (yU * zL + yL * zU) + yL * zL);
		error = (E) (other.error * error + repError + abs(other.value) * error + abs(value) * other.error);
		value = newValue;
		return *this;
	}

	Szam operator*(const Szam& other) const
	{
		return Szam(*this) *= other;
	}

	Szam operator/=(const Szam& other)
	{
		const V newValue = value / other.value;
		const V M = (V) (pow(2, (digits+1) / 2) + 1.0);
		const V yU = (newValue - newValue * M) + newValue * M, yL = newValue - yU, zU = (other.value - other.value * M) + other.value * M, zL = other.value - zU;
		const E repError = (E) abs((-newValue * other.value + value - (-newValue * other.value + yU * zU + (yU * zL + yL * zU) + yL * zL)) / other.value);
		error = (E) ((error + abs(newValue) * other.error) / max(abs(other.value) - other.error, 0.0) + repError);
		value = newValue;
		return *this;
	}

	Szam operator/(const Szam& other) const
	{
		return Szam(*this) /= other;
	}

	V getValue() const
	{
		return value;
	}

	E getError() const
	{
		return error;
	}

	E getRelError() const
	{
		return (E) ((value == 0.0 && error == 0.0) ? 0 : error / abs(value));
	}

};

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::RepEst, V, E> sqrt(const SZ<Method::RepEst, V, E>& x)
{
	const V newValue = (V) sqrt(x.getValue());
	const V M = (V) (pow(2.0, (SZ<Method::RepEst, V, E>::digits+1) / 2) + 1.0);
	const V xU = (newValue - newValue * M) + newValue * M, xL = newValue - xU;
	const E repError = (E) abs((-newValue * newValue + x.getValue() - (-newValue * newValue + xU * xU + (xU * xL + xL * xU) + xL * xL))) / (2.0 * newValue);
	const E error = (E) (x.getError() * 0.5 / newValue + repError);
	return SZ<Method::RepEst, V, E>(AbsoluteError(), newValue, error);
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::RepEst, V, E> abs(const SZ<Method::RepEst, V, E>& x)
{
	return SZ<Method::RepEst, V, E>(AbsoluteError(), (V) abs(x.getValue()), x.getError());
}


template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::RepEst, V, E> pow(const SZ<Method::RepEst, V, E>& x, const SZ<Method::RepEst, V, E>& y)
{
	//(a+alpha)^(b+beta)
	//beta * a^b * ln(a) + alpha * b * a ^ (b-1)
	const V value = (V) pow(x.getValue(), y.getValue());
	const E repError = (E) (pow(dd_real(x.getValue()), dd_real(y.getValue())) - value).toDouble();
	const E error = (E) (abs(repError) + abs(value) * y.getError() * abs(log(abs(x.getValue()))) + x.getError() * abs(y.getValue()) * abs(pow(x.getValue(), y.getValue() - 1)));
	return SZ<Method::RepEst, V, E>(AbsoluteError(), value, error);
}


template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::RepEst, V, E> log(const SZ<Method::RepEst, V, E>& x)
{
	const V value = (V) log(x.getValue());
	const E repError = (E) (log(dd_real(x.getValue())) - value).toDouble();
	const E error = (E) (x.getError() / abs(x.getValue()) + abs(repError));
	return SZ<Method::RepEst, V, E>(AbsoluteError(), value, error);
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::RepEst, V, E> cos(const SZ<Method::RepEst, V, E>& x)
{
	const V value = (V) cos(x.getValue());
	const E repError = (E) (cos(dd_real(x.getValue())) - value).toDouble();
	const E error = (E) (x.getError() * abs(sin(x.getValue())) + abs(repError));
	return SZ<Method::RepEst, V, E>(AbsoluteError(), value, error);
}

template<template <Method, class, class> class SZ, class V, class E>
SZ<Method::RepEst, V, E> sin(const SZ<Method::RepEst, V, E>& x)
{
	const V value = (V) sin(x.getValue());
	const E repError = (E) (sin(dd_real(x.getValue())) - value).toDouble();
	const E error = (E) (x.getError() * abs(cos(x.getValue())) + abs(repError));
	return SZ<Method::RepEst, V, E>(AbsoluteError(), value, error);
}