#pragma once

#include "ASTWrapper.hpp"

template <class U>
bool test(U result, U expected, const std::string& what = "No message... you lazy ****")
{
	if (result != expected)
	{
		std::cerr << "FAILED - " << what << std::endl;
		try {
			std::cout << "Got " << result << " expected " << expected << std::endl;
		}
		catch (...) {};
	}
	else
	{
		std::cout << "PASSED - " << what << std::endl;
	}

	return result == expected;
}

bool unit_tests()
{
	using D = AST<double>;

	bool isOK = true;

	//Constant, Variable, Bound constant variable, Bound AST variable, Bound AST variable with free params
	D one(1.0), a("a"), b("b"), c("c", 1.0), d("d", one * one), e("e", one * b), f(D(10) * D("x"));

	std::string test_dir = "tests/";

	////////////////////
	//EVALUATION TESTS//
	////////////////////

	isOK &= test(one.evaluate(), 1.0, "Constant evaluation check");
	isOK &= test(c.evaluate(), 1.0, "Bound variable evaluation check");
	isOK &= test(b.bound({ { "b", 1.0 } }).evaluate(), 1.0, "Variable binding evaluation check");
	isOK &= test(b.evaluate({ { "b", 1.0 } }), 1.0, "Variable evaluation with params");

	isOK &= test(d.evaluate(), 1.0, "Bound AST variable check");
	isOK &= test(e.evaluate({ { "b", 2.0 } }), 2.0, "Bound AST variable with free param check");

	f.integrate("x", 0, 1, 0.001);

	//not exact integration
	isOK &= test(f.evaluate(), 5.0000000000000044, "Integral evaluation check");

	////////////////
	//AST EQUALITY//
	////////////////

	isOK &= test((D(1) + D(1)) == (D(1) + D(1)), true, "AST equality check 1");
	isOK &= test((D(3) * D(1)) == (D(3) * D(1)), true, "AST equality check 2");
	isOK &= test((D("a") * D(1)) == (D("a") * D(1)), true, "AST equality check 3");
	isOK &= test((D("a") * D(1)) == (D("a") * D(1)), true, "AST equality check 4");
	isOK &= test((D("a") * D(1)) == (D("b") * D(1)), false, "AST equality check 5");
	isOK &= test((D("a") * D("b")).bound({ { "a", 2.0 },{ "b", 3.0 } }) == (D("a") * D("b")).bound({ { "b", 3.0 },{ "a", 2.0 } }), true, "AST equality check 6");

	///////////////////////
	//BINDING MERGE TESTS//
	///////////////////////

	D Bb3("b", 3.0), Bb4("b", 4.0), Ba7("a", 7.0);

	//Same Binding with same one Def should merge
	isOK &= test((Bb3 + Bb3).ast.bindingMerge({}, {}) == (b + b).bound({ { "b", 3.0 } }), true, "Simple same Binding merge check");

	//Same varible Binding with different Defs should not merge
	isOK &= test((Bb3 + Bb4).ast.bindingMerge({}, {}) == (Bb3 + Bb4).ast, true, "Simple same Binding Def name different Def value (non-)merge check");

	//The free variable should block the merge
	//Note, {"b"} should be given as lambda parameterset
	isOK &= test((Bb3 + b).ast.bindingMerge({}, { "b" }) == (Bb3 + b).ast, true, "Simple Binding vs free variable (non-)merge check");

	isOK &= test((Bb3 + Ba7).ast.bindingMerge({}, {}) == (b + a).bound({ { "a", 7.0 },{ "b", 3.0 } }).ast, true, "Different Binding merge check");

	//Same varible Binding with different Defs should not merge
	//Though Def_a should go to the top
	isOK &= test(((Bb3 + Ba7).ast.bindingMerge({}, {}) + Bb4.ast).bindingMerge({}, {}) \
		== ((b + a).bound({ { "b", 3.0 } }) + Bb4).bound({ { "a", 7.0 } }).ast, \
		true, "Same Binding Def name different Def value (non-)merge with one extra mergable Def check");

	isOK &= test(Bb3.bound({ { "a", 7.0 } }).ast.bindingMerge({}, {}) == b.bound({ { "b", 3.0 },{ "a", 7.0 } }).ast, \
		true, "Binding merge into Binding");

	isOK &= test(Bb3.bound({ { "b", 7.0 } }).ast.bindingMerge({}, {}) == b.bound({ { "b", 3.0 } }).bound({ { "b", 7.0 } }).ast, \
		true, "Binding (non-)merge into Binding");


	isOK &= test((a.bound({ { "a", D("c") } }).ast.bindingMerge({}, { "a", "c" }) * b.bound({ { "b", D("d") } }).ast.bindingMerge({}, { "b", "d" })).bindingMerge({}, { "c", "d" }).bound({ { "c", 2.0 },{ "d", 3.0 } }).bindingMerge({}, { "c", "d" }) \
		== (a*b).bound({ { "a", D("c") },{ "b", D("d") } }).bound({ { "c", 2.0 },{ "d", 3.0 } }), true, \
		"Variables bound to Defs depending on other Variables bound in an upper layer being merged");

	//SIMPLIFICATION

	isOK &= test(Bb3.simplify() == D(3.0), true, "One bound variable simplification");
	isOK &= test((Bb3 + Bb4).simplify() == D(7.0), true, "Sum of two bound variables simplified");

	isOK &= test(sqrt(b + D(4) * b).bound({ { "b", 5.0 } }).simplify() == D(5.0), true, "Expression with one bound variable simplification");

	isOK &= test(a.bound({ {"a", b.bound({{"b", D("c")}}) } }).simplify() == a.bound({ {"a", b} }).bound({ {"b", D("c")} }), true ,"a := b; b:=c Binding in Def simplification check");



	//TODO

	//Binding over Binding over Binding chain merge with itself

	//Binding Def with Binding merging above original top Binding
	//	multiple subDefs with same value
	//	multiple subDefs with different values
	//	multiple subDefs with partially same values
	//	multiple subDefs with partially same values with the AST leg Binding Defs
	//	multiple subDefs with partially same values with the AST leg Binding Defs and top Binding Defs -> some goes above, some goes into top, some stay down
	//	same with Binding chains included
	//	chains with break at the middle (another Def has the middle Binding Def set to something else)

	//VisuInfo<double> Dinfo(test_dir +  );
	return isOK;
}
